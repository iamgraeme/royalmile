<div class="col-md-5 home-sidebar">
    <aside id="sidebar">
        <ul>
            <?php
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('homepage')) :
                ?>
                <!-- Gift Experiences fallback content -->
                <?php
            endif;
            ?>
        </ul>
    </aside>
</div>