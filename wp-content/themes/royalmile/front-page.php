<?php get_header(); ?>

    <div class="container front-page">
        <blockquote>
            <span>A fun, creative recording studio experience... in the heart of Edinburgh's famous Royal Mile...</span>
        </blockquote>
    </div>

    <div class="container">
        <div class="row">
        <?php get_sidebar('home'); ?>
        <div id="main" class="col-md-7">
            <section>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <article <?php post_class(); ?>>
                        <?php the_content(); ?>
                    </article>
                <?php endwhile; ?>
                <?php else: ?>
                    <article>
                        <p>Nothing found! Terribly sorry!</p>
                    </article>
                <?php endif; ?>
            </section>
        </div>
        <div class="hidden-xl hidden-lg">
            <?php get_sidebar('home'); ?>
        </div>
        </div>
    </div>
<?php get_footer(); ?>