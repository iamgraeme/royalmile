<div class="col-sm-4">
    <aside id="sidebar-secondary">
        <ul>
            <?php
            global $post;
            if (is_page() && (($post->post_parent == 11) || ($post->ID == 11))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('giftexperiences')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 7) || ($post->ID == 7))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('henparties')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 9) || ($post->ID == 9))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('stagparties')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 17) || ($post->ID == 17))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('groupteam')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page('testimonials')) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('testimonials-secondary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            else:
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('secondary')) : ?>
                    <!-- Sidebar fallback content -->
                    <?php
                endif;
            endif; ?>
        </ul>
    </aside>
</div>