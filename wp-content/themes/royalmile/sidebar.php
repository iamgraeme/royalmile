<div class="col-md-4 sidebar-container hidden-xs hidden-sm">
    <aside id="sidebar">

        <?php $pageIdsPartyPackages = array(7366, 7549, 7375, 7377, 7381, 7387, 7389, 7555) ?>
        <ul>
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('global')) : ?>
                <?php
            endif;
            global $post;
            if (is_page() && (($post->post_parent == 11) || ($post->ID == 11))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('giftexperiences-primary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 7) || ($post->ID == 7))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('henparties-primary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 9) || ($post->ID == 9))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('stagparties-primary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->post_parent == 17) || ($post->ID == 17))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('groupteam-primary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && ((in_array($post->ID, $pageIdsPartyPackages)))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('party-package-2016')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_page() && (($post->ID == 7385) || ($post->ID == 7520) || ($post->ID == 7371))):
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('party-package-kids-page')) : ?>
                    <?php
                endif;
            elseif (is_page() && (($post->ID == 7383))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('party-package-hen-parties')) : ?>
                    <?php
                endif;
            elseif (is_page() && (($post->ID == 7392))) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('party-package-stag-parties')) : ?>
                    <?php
                endif;
            elseif (is_page('testimonials')) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('testimonials-primary')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            elseif (is_front_page()) :
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('homepage')) : ?>
                    <!-- Gift Experiences fallback content -->
                    <?php
                endif;
            else:

                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('primary')) : ?>
                    <!-- Sidebar fallback content -->
                    <?php
                endif;
            endif; ?>
        </ul>
    </aside>
</div>