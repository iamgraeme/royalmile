<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <title><?php wp_title(' &laquo; ', true, 'right'); ?></title>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="big-button-bg hidden-md hidden-lg" id="mobi-scroll-hide">
    <div class="hidden-md hidden-lg">
        <a href="<?php echo site_url('recording-studio-parties') ?>" class="la-btn button-yellow col-sm-12 col-xs-12">
            Studio Party Packages
        </a>
        <a href="<?php echo site_url('gift-experiences-vouchers') ?>" class="la-btn button-yellow col-sm-12 col-xs-12">
            Gift Experiences & Vouchers
        </a>
    </div>
</div>

<div id="sidr-right" class="sidr">
    <?php
    $args = array(
        'theme_location' => 'secondary',
        'fallback_cb' => false,
        'container' => false,
        'menu_id' => 'secondary-menu',
    );
    wp_nav_menu($args); ?>
</div>
<?php global $post;
if (is_page(array(4159, 4068, 4069, 11, 126, 6830, 7154)) || $post->ID == 4159): ?>
    <div class="top-bar">
        <div class="cart">
            <div class="container">
                <div class="build-now">
                    <a class="a-button buy-it-now" href="<?php echo site_url('product/star-experience') ?>">
                        Order Now
                    </a>
                </div>
                <a class="hidden-xs hidden-sm cart-total" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart'); ?>"><?php echo sprintf(_n('%d item', '%d items', WC()->cart->cart_contents_count), WC()->cart->cart_contents_count); ?> | <?php echo WC()->cart->get_cart_total(); ?></a>
            </div>
        </div>
    </div>
<?php elseif (is_page(array(3718, 147, 149, 140, 7, 9, 525, 528, 473, 507, 3416, 3426, 3931))): ?>
    <?php $options = get_option('appointedd_settings'); ?>
    <?php if ($options['appointedd_select_field_1']): ?>
        <div class="top-bar">
            <div class="cart">
                <div class="container">
                    <div class="build-now">
                        <a href="#" class="appointedd-booking-popover ap-popover-trigger"
                           ap-apikey="NTVkOWI0NGVmMDMyY2JhZjdmOGI0NWE4">
                            <?php echo $options['appointedd_text_field_0']; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

<div class="container header-container">
    <div id="contact" class="hidden-xs hidden-sm">
        <p class="smallererer">
             <span id="span_6ac1_0">
              <strong>Tel : 0131 556 4882</strong></span><br>
            <span id="span_6ac1_1">
                  <a href="mailto:info@royalmilerecordingexperiences.com"
                     onclick="_gaq.push(['_trackEvent', 'contact', 'sent', 'Header Email']);">
                      info@royalmilerecordingexperiences.com</a>
                  <br>
                 107 High Street, Royal Mile, Edinburgh EH1 1SW
              </span>
        </p>
    </div>
    <div class="hidden-md hidden-lg hidden-xl fl">
        <a href="<?php bloginfo('url'); ?>/location/" class="mobi-loc">
            <img src="<?php bloginfo('template_directory'); ?>/images/png/pin56.png" alt="Mobile Location">
        </a>
        <a href="tel:0131 556 4882" class="mobi-phone">
            <img src="<?php bloginfo('template_directory'); ?>/images/png/phone-receiver.png" alt="Mobile Telephone">
        </a>
    </div>
    <header class="site-header">
        <a href="<?php bloginfo('url'); ?>">
            <img src="<?php bloginfo('template_directory'); ?>/images/royalmilelogo.png" id="logo"
                 alt="<?php bloginfo('title') ?>"/>
            <?php if(is_front_page()): ?>
                <h1><strong><?php bloginfo('title') ?></strong></h1>
            <?php endif; ?>
        </a>
    </header>

    <div class="hidden-md hidden-lg hidden-xl fr">
        <a href="#sidr" id="sidr-menu">
            <img class="mobi-menu" src="<?php bloginfo('template_directory'); ?>/images/png/menu48.png"
                 alt="Mobile Menu">
        </a>
    </div>
    <div class="cb"></div>
    <a class="hidden-sm hidden-xs" href="<?php echo site_url('contact') ?>">
        <img src="<?php bloginfo('template_directory'); ?>/images/kirsty.png" alt="Recording Studio Parties"
             style="float:right; margin-left:0; height:auto; margin-bottom:1em;">
    </a>
    <nav class="hidden-sm hidden-xs">
        <?php
        $args = array(
            'theme_location' => 'secondary',
            'fallback_cb' => false,
            'container' => false,
            'menu_id' => 'secondary-menu',
            'menu_class' => "nav-menu-2"
        );
        wp_nav_menu($args);


        $args = array(
            'theme_location' => 'primary',
            'fallback_cb' => false,
            'container' => false,
            'depth' => 1,
            'menu_id' => 'menu',
            'menu_class' => "nav-menu"
        );
        wp_nav_menu($args);

        ?>
    </nav>
</div>
<div class="visible-sm visible-xs mobile-phone center">
    <a href="tel:01315564882">Tel : 0131 556 4882</a>
    <br>
    <a href="mailto:info@royalmilerecordingexperiences.com"
       onclick="_gaq.push(['_trackEvent', 'contact', 'sent', 'Header Email']);">
        info@royalmilerecordingexperiences.com</a>
</div>


<?php if (function_exists('breadcrumb_trail')): ?>
<?php if (!is_front_page()): ?>
    <div class="breadcrumb-container hidden-xs hidden-sm">
        <div class="container breadcrumb">
            <?php breadcrumb_trail(); ?>
        </div>
    </div>
<?php endif; ?>
<?php endif; ?>