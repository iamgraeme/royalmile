var app = function ($) {

    var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

    function fnBlockQuoteHelper() {

        var blockQuote = $('blockquote');

        blockQuote.prepend('<div class="before"></div>');
        blockQuote.append('<div class="after"></div>');
    }

    function fnStickySidebar() {
        var sidebar = $("#sidebar");

        sidebar.stick_in_parent({
            offset_top: 20
        });
    }

    function fnRemoveAhref() {

        var listItem = $('.page-item-7394');

        listItem.find("a").removeAttr("href");
    }

    function init() {
        fnBlockQuoteHelper();
        fnStickySidebar();
        fnRemoveAhref();
    }

    return {
        init: init
    };

}(jQuery);

jQuery(document).ready(function () {
    app.init();
});


// handle links with @href started with '#' only
jQuery(document).on('click', 'a[href^="#"]', function (e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $(id).offset().top - 100;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});


var w = $(window),
    d = $(document),
    elem = $('#mobi-scroll-hide');

w.scroll(function () {
    var _this = $(this);

    if (_this.scrollTop() > 0) {
        elem.slideUp('fast');
    }
    else {
        elem.slideDown('fast');
    }
});

w.scroll(function () {
    if (w.scrollTop() + w.height() == d.height()) {
        elem.slideDown('fast');
    }
});


function sticky_relocate() {

    var window_top = $(window).scrollTop(),
        sticky = $('#sticky'),
        stickyAnchor = $('#sticky-anchor'),
        div_top = stickyAnchor.offset().top;

    if (window_top > div_top) {
        sticky.addClass('stick');
        stickyAnchor.height(sticky.outerHeight());
    } else {
        sticky.removeClass('stick');
        stickyAnchor.height(0);
    }
}

$(function () {
    w.scroll(sticky_relocate);
    sticky_relocate();
});

