<div class="container">
   <a class="hidden-md hidden-lg" href="tel:01315564882">
       <img src="<?php bloginfo('template_directory'); ?>/images/kirsty.png" alt="Recording Studio Parties"
             style="margin-right:auto; margin-left:auto; height:auto; margin-bottom:1em; display:block;">
    </a>
    <footer>
        <?php
        $args = array(
            'theme_location' => 'secondary',
            'fallback_cb' => false,
            'container' => false,
            'menu_id' => 'secondary-menu',
            'menu_class' => 'nav-menu-2'

        );
        wp_nav_menu($args);
        ?>
        <div class="geo-trust-site-seal fr">
            <!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
            <script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
            <!-- end  GeoTrust Smart Icon tag -->
        </div>
        <div class="cb"></div>
    </footer>

    <div class="social-accounts">
        <ul class="fl">
            <li><a href="https://www.facebook.com/Royal-Mile-Recording-Experiences-602234196472505/" target="_blank">
                    <i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCy6-zb-mdQHUBkinYjRNp7w" target="_blank">
                    <i class="fa fa-youtube"></i></a></li>
            <li><a href="https://soundcloud.com/user1233773" target="_blank">
                    <i class="fa fa-soundcloud"></i></a></li>

            <li><a href="https://twitter.com/offbeatscotland" target="_blank">
                    <i class="fa fa-twitter"></i></a></li>

            <li><a href="https://uk.linkedin.com/in/iainmckinna" target="_blank">
                    <i class="fa fa-linkedin"></i></a></li>
        </ul>

        <div class="fr"><p>&copy; <?php echo date('Y') ?> Royal Mile Recordings</p></div>
    </div>
</div>
<?php wp_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var sidrMenu = jQuery('#sidr-menu');

        sidrMenu.sidr({
            name: 'sidr-right',
            side: 'right'
        });

    });
</script>

<script language="javascript" type="text/javascript"
        src="//s3-eu-west-1.amazonaws.com/appointedd-portal-assets/popover/appointedd-button.js">
</script>
</body>
</html>