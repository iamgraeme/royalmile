<?php
/*
Template Name: FAQs
*/
?>
<?php get_header(); ?>

<div class="container">
  <div id="main">
    <section>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article class="<?php post_class();?>">
          <h2><?php the_title(); ?></h2>
          <?php the_content(); ?>
        </article>

      <?php endwhile; ?>
      <?php else: ?>
        <p>Nothing found! Terribly sorry!</p>
      <?php endif; ?>
      <nav><?php posts_nav_link(' &infin; ','1985 &rarr;','&larr; 1955'); ?></nav>

    </section>
    <?php get_sidebar('secondary'); ?>
  </div>
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>