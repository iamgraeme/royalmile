<?php get_header(); ?>

<div class="container">
  <?php get_sidebar(); ?>
  <div id="main">
    <section>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article <?php post_class();?>>
          <h1><?php the_title(); ?></h1>
          <?php the_content(); ?>

          <div class="post-pages">
            <?php wp_link_pages( $args ); ?>
          </div>
        </article>

      <?php endwhile; ?>
      <?php else: ?>
        <p>Nothing found! Terribly sorry!</p>
      <?php endif; ?>
      <nav><?php posts_nav_link(); ?></nav>

    </section>
    <?php get_sidebar('secondary'); ?>

  </div>
</div>

<?php get_footer(); ?>
