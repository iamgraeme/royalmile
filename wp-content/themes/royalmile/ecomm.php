<?php
/*
Template Name: Ecommerce
*/
?>

<?php get_header(); ?>

<div class="container">
    <?php get_sidebar(); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <article <?php post_class(); ?>>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>

                <div class="post-pages">
                    <?php wp_link_pages($args); ?>
                </div>

                <?php get_template_part('_addthis'); ?>
                <?php comments_template(); ?>
            </article>

        <?php endwhile; ?>
        <?php else: ?>
            <p>Nothing found! Terribly sorry!</p>
        <?php endif; ?>
</div>

<?php get_footer(); ?>

