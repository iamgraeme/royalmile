<?php

function load_rmre_scripts()
{
    $url = '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css';
    $cdn = wp_remote_get($url);
    if ((int)wp_remote_retrieve_response_code($cdn) !== 200) {
        $url = get_template_directory_uri() . '/stylesheets/font-awesome.min.css';
    }
    wp_enqueue_style('font-awesome', $url, false);
    wp_enqueue_style(
        'twitter-bootstrap',
        get_template_directory_uri() . '/stylesheets/bootstrap.css');
    wp_enqueue_script('jquery-sidr', get_template_directory_uri() . '/js/jquery.sidr.min.js', false, false, true);
    wp_enqueue_script('custom-js', get_template_directory_uri() . '/javascripts/application.js', false, false, true);
    wp_enqueue_script('core-js', get_template_directory_uri() . '/javascripts/core.js', false, false, true);
    wp_enqueue_script('sticky-kit', get_template_directory_uri() . '/javascripts/jquery.sticky-kit.min.js', false, false, true);
    wp_enqueue_script('headroom', get_template_directory_uri() . '/javascripts/headroom.min.js', false, false, true);
    wp_enqueue_style('sidrstyle', get_template_directory_uri() . '/stylesheets/jquery.sidr.dark.css', false);
    wp_enqueue_style('my-style', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'load_rmre_scripts');

register_sidebar(array(
    'name' => 'Global Primary',
    'id' => 'global',
    'description' => 'At the top of each primary',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Primary',
    'id' => 'primary',
    'description' => 'The main sidebar for the site',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Secondary',
    'id' => 'secondary',
    'description' => 'The secondary sidebar for the site',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Gift Experiences Primary',
    'id' => 'giftexperiences-primary',
    'description' => 'The secondary sidebar for the gift experiences section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Gift Experiences',
    'id' => 'giftexperiences',
    'description' => 'The secondary sidebar for the gift experiences section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Stag Parties Primary',
    'id' => 'stagparties-primary',
    'description' => 'The secondary sidebar for the stag parties section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Stag Parties',
    'id' => 'stagparties',
    'description' => 'The secondary sidebar for the stag parties section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Hen Parties Primary',
    'id' => 'henparties-primary',
    'description' => 'The secondary sidebar for the hen parties section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Hen Parties',
    'id' => 'henparties',
    'description' => 'The secondary sidebar for the hen parties section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Group / Team Primary',
    'id' => 'groupteam-primary',
    'description' => 'The secondary sidebar for the group / team section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));


register_sidebar(array(
    'name' => 'Group / Team',
    'id' => 'groupteam',
    'description' => 'The secondary sidebar for the group / team section',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Homepage',
    'id' => 'homepage',
    'description' => 'Sidebar for Homepage',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Testimonials Primary',
    'id' => 'testimonials-primary',
    'description' => 'Primary sidebar on testimonials page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Party Package 2016-17 Sidebar',
    'id' => 'party-package-2016',
    'description' => 'Primary sidebar on party packages page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Party Package Kids',
    'id' => 'party-package-kids-page',
    'description' => 'Primary sidebar on kids parties page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Party Package Hen',
    'id' => 'party-package-hen-parties',
    'description' => 'Primary sidebar on hen parties page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Party Package Stag',
    'id' => 'party-package-stag-parties',
    'description' => 'Primary sidebar on stag parties page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_sidebar(array(
    'name' => 'Testimonials Secondary',
    'id' => 'testimonials-secondary',
    'description' => 'Primary sidebar on testimonials page',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>\n",
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => "</h2>\n"
));

register_nav_menu('primary', 'Primary Navigation');
register_nav_menu('secondary', 'Secondary Navigation');

// Allow shortcodes in text widgets.
add_filter('widget_text', 'do_shortcode');

function is_child($page_id)
{
    global $post;
    if (is_page() && ($post->post_parent == $page_id)) {
        return true;
    } else {
        return false;
    }
}

/**
 *
 * Add WooCommerce Support
 *
 **/


add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

// remove cross-sells from their normal place
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
// add them back in further up the page
add_action('woocommerce_before_cart_table', 'woocommerce_cross_sell_display');

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);
function custom_variation_price($price, $product)
{
    $price = '';
    if (!$product->min_variation_price || $product->min_variation_price !== $product->max_variation_price) $price .= '<span class="from">' . _x('From', 'min_price', 'woocommerce') . ' </span>';
    $price .= woocommerce_price($product->min_variation_price);
    return $price;
}

add_filter('woocommerce_variable_sale_price_html', 'wc_wc20_variation_price_format', 10, 2);
add_filter('woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2);
function wc_wc20_variation_price_format($price, $product)
{
    // Main Price
    $prices = array($product->get_variation_price('min', true), $product->get_variation_price('max', true));
    $price = $prices[0] !== $prices[1] ? sprintf(__('From: %1$s', 'woocommerce'), wc_price($prices[0])) : wc_price($prices[0]);

    // Sale Price
    $prices = array($product->get_variation_regular_price('min', true), $product->get_variation_regular_price('max', true));
    sort($prices);
    $saleprice = $prices[0] !== $prices[1] ? sprintf(__('From: %1$s', 'woocommerce'), wc_price($prices[0])) : wc_price($prices[0]);
    if ($price !== $saleprice) {
        $price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
    }
    return $price;
}

add_action('admin_menu', 'appointedd_add_admin_menu');
add_action('admin_init', 'appointedd_settings_init');


function appointedd_add_admin_menu()
{

    add_menu_page('Appointedd', 'Appointedd', 'manage_options', 'appointedd', 'appointedd_options_page');

}


function appointedd_settings_init()
{

    register_setting('pluginPage', 'appointedd_settings');

    add_settings_section(
        'appointedd_pluginPage_section',
        __('Appointedd Settings', 'appointedd'),
        'appointedd_settings_section_callback',
        'pluginPage'
    );

    add_settings_field(
        'appointedd_text_field_0',
        __('Appointedd Button Text', 'appointedd'),
        'appointedd_text_field_0_render',
        'pluginPage',
        'appointedd_pluginPage_section'
    );

    add_settings_field(
        'appointedd_select_field_1',
        __('Enable Appointedd?', 'appointedd'),
        'appointedd_select_field_1_render',
        'pluginPage',
        'appointedd_pluginPage_section'
    );


}


function appointedd_text_field_0_render()
{

    $options = get_option('appointedd_settings');
    ?>
    <input type='text' name='appointedd_settings[appointedd_text_field_0]'
           value='<?php echo $options['appointedd_text_field_0']; ?>'>
    <?php

}


function appointedd_select_field_1_render()
{

    $options = get_option('appointedd_settings');
    ?>
    <select name='appointedd_settings[appointedd_select_field_1]'>
        <option value='0' <?php selected($options['appointedd_select_field_1'], 0); ?>>No</option>
        <option value='1' <?php selected($options['appointedd_select_field_1'], 1); ?>>Yes</option>
    </select>

    <?php

}


function appointedd_settings_section_callback()
{

    echo __('This is where you can configure appointedd settings', 'appointedd');

}


function appointedd_options_page()
{

    ?>
    <form action='options.php' method='post'>

        <h2>Appointedd</h2>

        <?php
        settings_fields('pluginPage');
        do_settings_sections('pluginPage');
        submit_button();
        ?>

    </form>
    <?php


}


// Ensure cart contents update when products are added to the cart via AJAX
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    ?>
    <a class="hidden-xs hidden-sm cart-total" href="<?php echo WC()->cart->get_cart_url(); ?>"
       title="<?php _e('View your shopping cart'); ?>"><?php echo sprintf(_n('%d item', '%d items', WC()->cart->cart_contents_count), WC()->cart->cart_contents_count); ?>
        | <?php echo WC()->cart->get_cart_total(); ?></a>
    <?php

    $fragments['a.cart-total'] = ob_get_clean();

    return $fragments;
}

remove_action('admin_notices', 'woothemes_updater_notice');


add_filter('manage_posts_columns', 'posts_columns_postpageID', 5);
add_action('manage_posts_custom_column', 'posts_custom_columns_postpageID', 5, 2);
add_filter('manage_pages_columns', 'posts_columns_postpageID', 5);
add_action('manage_pages_custom_column', 'posts_custom_columns_postpageID', 5, 2);

function posts_columns_postpageID($defaults)
{
    $defaults['wps_post_id'] = __('Page ID');
    return $defaults;
}

function posts_custom_columns_postpageID($column_name, $id)
{
    if ($column_name === 'wps_post_id') {
        echo $id;
    }

}




//function wpdocs_delete_pages() {
//    $posts_to_delete = array(7833,7555,7549,6837,3931,3718,3432,3416,3412,2697,2253,2116,2008,531,528,525,513,510,507,473,151,149,147,142,140,132,130,17,9,7);
//
//    foreach ( $posts_to_delete as $myproduct ) {
//
//        wp_trash_post( $myproduct, true);
//    }
//}
//add_action( 'init', 'wpdocs_delete_pages' );