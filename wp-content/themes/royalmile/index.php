<?php get_header(); ?>

<div class="container">
    <div class="row">
    <?php get_sidebar(); ?>
    <div id="main" class="col-md-8">
        <section>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article <?php post_class(); ?>>
                    <?php if (is_single() || is_page() && !is_front_page()) : ?>

                        <h1><?php the_title(); ?></h1>
                        <?php the_content(); ?>

                        <div class="post-pages">
                            <?php wp_link_pages($args); ?>
                        </div>

                        <?php
                        if (is_single()) :
                            get_template_part('_addthis');
                        endif;
                        ?>

                        <?php if (!is_page()) : ?>
                            <div class="meta">
                                <p>Posted on <?php the_time('l, F jS, Y') ?>.</p>

                                <p class="tags"><?php the_tags(); ?></p>

                                <p class="categories">Categories: <?php the_category(' '); ?></p>
                            </div>
                        <?php endif; ?>

                        <?php if (is_single()) : ?>
                            <?php comments_template(); ?>
                        <?php endif; ?>

                    <?php elseif (is_front_page()) : ?>

                        <?php the_content(); ?>

                    <?php else : ?>

                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>

                    <?php endif; ?>
                </article>

            <?php endwhile; ?>
            <?php else: ?>
                <p>Nothing found! Terribly sorry!</p>
            <?php endif; ?>
            <nav><?php posts_nav_link(); ?></nav>

        </section>
    </div>
</div>
</div>

<?php get_footer(); ?>
