<?php
/**
 * Plugin Name: Responsive Pricing Table PRO
 * Plugin URI: http://wpdarko.com/responsive-pricing-table-pro/
 * Description: A responsive, easy and elegant way to present your offer to your visitors. Create a new pricing table and copy-paste the shortcode into your posts/pages. Find help and information on our <a href="http://wpdarko.com/support/">support site</a>. Make sure you check out all our useful themes and plugins at <a href='http://wpdarko.com/'>WPDarko.com</a>.
 * Version: 4.3
 * Author: WP Darko
 * Author URI: http://wpdarko.com/
 * Text Domain: responsive-pricing-table
 * Domain Path: /lang/
 * License: GPL2
 */


 // Loading text domain
 add_action( 'plugins_loaded', 'rptp_load_plugin_textdomain' );
 function rptp_load_plugin_textdomain() {
   load_plugin_textdomain( 'responsive-pricing-table', FALSE, basename( dirname( __FILE__ ) ) . '/lang/' );
 }


//Checking for updates
define( 'rpt_STORE_URL', 'https://wpdarko.com' );
define( 'rpt_ITEM_NAME', 'Responsive Pricing Table' );

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	include( dirname( __FILE__ ) . '/inc/darko_updater/darko_updater.php' );
}

function rpt_sl_plugin_updater() {

	$license_key = trim( get_option( 'rpt_license_key' ) );

	$rpt_updater = new EDD_SL_Plugin_Updater( rpt_STORE_URL, __FILE__, array(
			'version' 	=> '4.3',
			'license' 	=> $license_key,
			'item_name' => rpt_ITEM_NAME,
			'author' 	=> 'WP Darko'
		)
	);

}
add_action( 'admin_init', 'rpt_sl_plugin_updater', 0 );

//Adding license menu
function rpt_license_menu() {
	add_submenu_page( 'edit.php?post_type=rpt_pricing_table', 'Responsive Pricing Table PRO', __( 'License activation', 'responsive-pricing-table' ), 'manage_options', 'responsive-pricing-table-license', 'rpt_license_page' );
}
add_action('admin_menu', 'rpt_license_menu');

function rpt_license_page() {
	$license 	= get_option( 'rpt_license_key' );
	$status 	= get_option( 'rpt_license_status' );
	?>
	<div class="wrap">
        <div style="background:white; padding:0px 20px; margin-top:20px; padding-top:10px;">
        <h2>Responsive Pricing Table </h2>
        <h3 style="color:lightgrey;"><span style="color:lightgrey;" class="dashicons dashicons-admin-network"></span>&nbsp; <?php echo __( 'Activate your license', 'responsive-pricing-table' ); ?></h2>
        <p style="color:grey;"><?php echo __( 'Activating your license allows you to receive automatic updates for a year.<br/>This is <strong>NOT</strong> required for the plugin to properly work.', 'responsive-pricing-table' ); ?></p>
		<form method="post" action="options.php">

			<?php settings_fields('rpt_license'); ?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php echo __( 'License Key', 'responsive-pricing-table' ); ?>
						</th>
						<td>
							<input id="rpt_license_key" name="rpt_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
							<label class="description" for="rpt_license_key"></label>
						</td>
					</tr>
					<?php if( false !== $license ) { ?>
						<tr valign="top">
							<th scope="row" valign="top">
								<?php echo __( 'Activate license', 'responsive-pricing-table' ); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<span style="color:green; line-height:29px; margin-right:20px;"><span style="line-height:30px;" class="dashicons dashicons-yes"></span><?php echo __( 'Activated', 'responsive-pricing-table' ); ?></span>
									<?php wp_nonce_field( 'rpt_nonce', 'rpt_nonce' ); ?>
									<input type="submit" class="button-secondary" name="rpt_license_deactivate" value="<?php echo __( 'Deactivate license', 'responsive-pricing-table' ); ?>"/>
								<?php } else {
									wp_nonce_field( 'rpt_nonce', 'rpt_nonce' ); ?>
									<input type="submit" class="button-secondary" name="rpt_license_activate" value="<?php echo __( 'Activate license', 'responsive-pricing-table' ); ?>"/>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php submit_button(); ?>

		</form>
        </div>
	<?php
}

function rpt_register_option() {
	register_setting('rpt_license', 'rpt_license_key', 'rpt_sanitize_license' );
}
add_action('admin_init', 'rpt_register_option');

function rpt_sanitize_license( $new ) {
	$old = get_option( 'rpt_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'rpt_license_status' );
	}
	return $new;
}

function rpt_activate_license() {

	if( isset( $_POST['rpt_license_activate'] ) ) {

	 	if( ! check_admin_referer( 'rpt_nonce', 'rpt_nonce' ) )
			return;

		$license = trim( get_option( 'rpt_license_key' ) );

		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $license,
			'item_name' => urlencode( rpt_ITEM_NAME ),
			'url'       => home_url()
		);

		$response = wp_remote_get( add_query_arg( $api_params, rpt_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

		if ( is_wp_error( $response ) )
			return false;

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		update_option( 'rpt_license_status', $license_data->license );
	}
}
add_action('admin_init', 'rpt_activate_license');

function rpt_deactivate_license() {

	if( isset( $_POST['rpt_license_deactivate'] ) ) {

	 	if( ! check_admin_referer( 'rpt_nonce', 'rpt_nonce' ) )
			return;

		$license = trim( get_option( 'rpt_license_key' ) );

		$api_params = array(
			'edd_action'=> 'deactivate_license',
			'license' 	=> $license,
			'item_name' => urlencode( rpt_ITEM_NAME ),
			'url'       => home_url()
		);

		$response = wp_remote_get( add_query_arg( $api_params, rpt_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

		if ( is_wp_error( $response ) )
			return false;

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		if( $license_data->license == 'deactivated' )
			delete_option( 'rpt_license_status' );

	}
}
add_action('admin_init', 'rpt_deactivate_license');


/* Enqueue styles */
add_action( 'wp_enqueue_scripts', 'add_rptp_scripts', 99 );
function add_rptp_scripts() {
	wp_enqueue_style( 'rpt', plugins_url('css/rpt_style.min.css', __FILE__));
}


/* Enqueue admin styles */
add_action( 'admin_enqueue_scripts', 'add_admin_rptp_style' );
function add_admin_rptp_style() {
	global $post_type;
	if( 'rpt_pricing_table' == $post_type ) {
			wp_enqueue_style( 'rpt', plugins_url('css/admin_de_style.min.css', __FILE__));
			wp_enqueue_script( 'rpt', plugins_url('js/rpt_admin.min.js', __FILE__), array( 'jquery' ));
	}
}


// Register Pricing Table post type
add_action( 'init', 'register_rptp_type' );
function register_rptp_type() {
	$labels = array(
		'name'               => __( 'Pricing Tables', 'responsive-pricing-table' ),
		'singular_name'      => __( 'Pricing Table', 'responsive-pricing-table' ),
		'menu_name'          => __( 'Pricing Tables', 'responsive-pricing-table' ),
		'name_admin_bar'     => __( 'Pricing Table', 'responsive-pricing-table' ),
		'add_new'            => __( 'Add New', 'responsive-pricing-table' ),
		'add_new_item'       => __( 'Add New Pricing Table', 'responsive-pricing-table' ),
		'new_item'           => __( 'New Pricing Table', 'responsive-pricing-table' ),
		'edit_item'          => __( 'Edit Pricing Table', 'responsive-pricing-table' ),
		'view_item'          => __( 'View Pricing Table', 'responsive-pricing-table' ),
		'all_items'          => __( 'All Pricing Tables', 'responsive-pricing-table' ),
		'search_items'       => __( 'Search Pricing Tables', 'responsive-pricing-table' ),
		'not_found'          => __( 'No Pricing Tables found.', 'responsive-pricing-table' ),
		'not_found_in_trash' => __( 'No Pricing Tables found in Trash.', 'responsive-pricing-table' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
        'show_in_admin_bar'  => false,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-plus'
	);
	register_post_type( 'rpt_pricing_table', $args );
}


// Customize update messages
add_filter( 'post_updated_messages', 'rptp_updated_messages' );
function rptp_updated_messages( $messages ) {
	$post             = get_post();
	$post_type        = get_post_type( $post );
	$post_type_object = get_post_type_object( $post_type );
	$messages['rpt_pricing_table'] = array(
		1  => __( 'Pricing Table updated.', 'responsive-pricing-table' ),
		4  => __( 'Pricing Table updated.', 'responsive-pricing-table' ),
		6  => __( 'Pricing Table published.', 'responsive-pricing-table' ),
		7  => __( 'Pricing Table saved.', 'responsive-pricing-table' ),
		10 => __( 'Pricing Table draft updated.', 'responsive-pricing-table' )
	);

	if ( $post_type_object->publicly_queryable ) {
		$permalink = get_permalink( $post->ID );

		$view_link = sprintf( '', '', '' );
		$messages[ $post_type ][1] .= $view_link;
		$messages[ $post_type ][6] .= $view_link;
		$messages[ $post_type ][9] .= $view_link;

		$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
		$preview_link = sprintf( '', '', '' );
		$messages[ $post_type ][8]  .= $preview_link;
		$messages[ $post_type ][10] .= $preview_link;
	}
	return $messages;
}


// Add the amazing metabox class (CMB2)
if ( file_exists( dirname( __FILE__ ) . '/inc/cmb2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/inc/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/inc/CMB2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/inc/CMB2/init.php';
}


// Registering Pricing Table metaboxes
add_action( 'cmb2_init', 'rptp_register_group_metabox' );
require_once('inc/rpt-metaboxes.php');


// Shortcode column
add_action( 'manage_rpt_pricing_table_posts_custom_column' , 'rptp_custom_columns', 10, 2 );
add_filter('manage_rpt_pricing_table_posts_columns' , 'add_rptp_pricing_table_columns');
function rptp_custom_columns( $column, $post_id ) {
    switch ( $column ) {
	case 'dk_shortcode' :
		global $post;
		$slug = '' ;
		$slug = $post->post_name;
        $shortcode = '<span style="display:inline-block;border:solid 2px lightgray; background:white; padding:0 8px; font-size:13px; line-height:25px; vertical-align:middle;">[rpt name="'.$slug.'"]</span>';
	    echo $shortcode;
	    break;
    }
}
function add_rptp_pricing_table_columns($columns) { return array_merge($columns, array('dk_shortcode' => 'Shortcode'));}


// Display Shortcode
add_shortcode("rpt", "rptpro_sc");
function rptpro_sc($atts) {
	extract(shortcode_atts(array(
		"name" => '',
	), $atts));
	$output2 = '';

  global $post;

  $args = array('post_type' => 'rpt_pricing_table', 'name' => $name);
  $custom_posts = get_posts($args);
  foreach($custom_posts as $post) : setup_postdata($post);

	$entries = get_post_meta( $post->ID, '_rpt_plan_group', true );

	$nb_entries = count($entries);

    // Forcing original fonts?
    $original_font = get_post_meta( $post->ID, '_rpt_original_font', true );
    if ($original_font == true){
        $ori_f = 'rpt_plan_ori';
    } else {
        $ori_f = '';
    }

	/* --- Get font sizes --- */
	$title_fontsize = get_post_meta( $post->ID, '_rpt_title_fontsize', true );
	if ($title_fontsize == 'small') {
		$title_fs_class = ' rpt_sm_title';
	} else if ($title_fontsize == 'tiny') {
		$title_fs_class = ' rpt_xsm_title';
	} else {
		$title_fs_class = '';
	}

	$subtitle_fontsize = get_post_meta( $post->ID, '_rpt_subtitle_fontsize', true );
	if ($subtitle_fontsize == 'small') {
		$subtitle_fs_class = ' rpt_sm_subtitle';
	} else if ($subtitle_fontsize == 'tiny') {
		$subtitle_fs_class = ' rpt_xsm_subtitle';
	} else {
		$subtitle_fs_class = '';
	}

	$description_fontsize = get_post_meta( $post->ID, '_rpt_description_fontsize', true );
	if ($description_fontsize == 'small') {
		$description_fs_class = ' rpt_sm_description';
	} else {
		$description_fs_class = '';
	}

	$price_fontsize = get_post_meta( $post->ID, '_rpt_price_fontsize', true );
	if ($price_fontsize == 'small') {
		$price_fs_class = ' rpt_sm_price';
	} else if ($price_fontsize == 'tiny') {
		$price_fs_class = ' rpt_xsm_price';
    } else if ($price_fontsize == 'supertiny') {
		$price_fs_class = ' rpt_xxsm_price';
	} else {
		$price_fs_class = '';
	}

	$recurrence_fontsize = get_post_meta( $post->ID, '_rpt_recurrence_fontsize', true );
	if ($recurrence_fontsize == 'small') {
		$recurrence_fs_class = ' rpt_sm_recurrence';
	} else {
		$recurrence_fs_class = '';
	}

	$features_fontsize = get_post_meta( $post->ID, '_rpt_features_fontsize', true );
	if ($features_fontsize == 'small') {
		$features_fs_class = ' rpt_sm_features';
	} else {
		$features_fs_class = '';
	}

	$button_fontsize = get_post_meta( $post->ID, '_rpt_button_fontsize', true );
	if ($button_fontsize == 'small') {
		$button_fs_class = ' rpt_sm_button';
	} else {
		$button_fs_class = '';
	}

    /* --- Get pricing table style from options --- */
	$skin = get_post_meta( $post->ID, '_rpt_skin', true );
	if ($skin == 'lottacolor'){
		$current_skin = 'rpt_style_loc';
	} else if ($skin == 'spec') {
		$current_skin = 'rpt_style_spec';
	} else if ($skin == 'bitcolor') {
		$current_skin = 'rpt_style_bic';
	} else if ($skin == 'sweetlines') {
		$current_skin = 'rpt_style_swl';
	} else {
		$current_skin = 'rpt_style_basic';
	}

    /* --- Opening rpt container --- */
	$output2 .= '<div id="rpt_container" class="rpt_plans rpt_'.$nb_entries .'_plans '. $current_skin .'">';

	   /* --- Opening rpt inner --- */
	   $output2 .= '<div class="rpt_inner '. $title_fs_class . $subtitle_fs_class . $description_fs_class . $price_fs_class . $recurrence_fs_class .   $features_fs_class. $button_fs_class .'">';

    if (is_array($entries) || is_object($entries))
	   foreach ($entries as $key => $plans) {

	   if (!isset($plans['_rpt_color'])) {
		  $plans['_rpt_color'] = '#9FDB80';
	   }

	   if (empty($plans['_rpt_color'])) {
	   	$plans['_rpt_color'] = '#9FDB80';
	   }

	   /* --- Check if recommended plan --- */
	   if (!empty($plans['_rpt_recommended'])){
	   	$is_reco = $plans['_rpt_recommended'];
	   	/* --- Opening plan --- */
	   	if ($is_reco == true ){
	   		/* check for skin */
	   		if ($skin == 'lottacolor' || $skin == 'sweetlines'){
	   		   $reco = '<img class="rpt_recommended" src="' . plugins_url('img/rpt_recommendedw.png', __FILE__) . '"/>';
	   		} else {
	   		   $reco = '<img class="rpt_recommended" src="' . plugins_url('img/rpt_recommended.png', __FILE__) . '"/>';
	   		}
	   		/* --- Add the recommended class --- */
	   	    $reco_class = 'rpt_recommended_plan';
	   	} else if ($is_reco == false ) {
	   	    $reco = '';
	   	    $reco_class = '';
	   	}
	   } else {
	   	$reco = '';
	   	$reco_class = '';
	   }

     if (empty($plans['_rpt_custom_classes'])){
       $plans['_rpt_custom_classes'] = '';
     }

	   if ($skin == 'sweetlines'){
	   	$output2 .= '<div style="border:4px solid ' . $plans['_rpt_color'] . ';" class="rpt_plan '.$ori_f.' rpt_plan_' . $key . ' ' . $reco_class . ' ' . $plans['_rpt_custom_classes'] . '">';
	   } else {
	   	$output2 .= '<div class="rpt_plan '.$ori_f.' rpt_plan_' . $key . ' ' . $reco_class . ' ' . $plans['_rpt_custom_classes'] . '">';
	   }

	   	/* --- Plan title --- */
	   	if ($skin == 'lottacolor'){
	   		$title_style = 'style="opacity:0.8; background-color:' . $plans['_rpt_color'] . ';"';
	   	} else if ($skin == 'sweetlines') {
	   		$title_style = 'style="background-color:' . $plans['_rpt_color'] . ';"';
	   	} else {
	   		$title_style = '';
	   	}
	   	if (!empty($plans['_rpt_title'])){
	   		$output2 .= '<div ' . $title_style . ' class="rpt_title rpt_title_' . $key . '">';

	   		if (!empty($plans['_rpt_icon'])){
	   			$output2 .= '<img height=30px width=30px src="' . $plans['_rpt_icon'] . '" class="rpt_icon rpt_icon_' . $key . '"/> ';
	   		}

	   		$output2 .= $plans['_rpt_title'];
	   		$output2 .= $reco . '</div>';
	   	}

	   	/* --- Plan header (box below the title) --- */
	   	if ($skin == 'lottacolor' || $skin == 'bitcolor'){
	   		$head_style = 'style="background-color:' . $plans['_rpt_color'] . ';"';
	   	} else {
	   		$head_style = '';
	   	}

	   	$output2 .= '<div ' . $head_style . ' class="rpt_head rpt_head_' . $key . '">';

	   		/* --- Plan recurrence --- */
	   		if (!empty($plans['_rpt_recurrence'])){
	   		    	$output2 .= '<div class="rpt_recurrence rpt_recurrence_' . $key . '">' . $plans['_rpt_recurrence'] . '</div>';
	   		}

	   		/* --- Plan price background color if 'sweetlines' skin --- */
	   		if ($skin == 'sweetlines'){
	   		    $output2 .= '<div style="color:' . $plans['_rpt_color'] . '" class="rpt_price rpt_price_' . $key . '">';
	   		} else {
	   		    $output2 .= '<div class="rpt_price rpt_price_' . $key . '">';
	   		}

            /* --- Check if the plan is free and add currency if not free --- */
	   		if (!empty($plans['_rpt_free'])){
			    	if ($plans['_rpt_free'] == true ){
			    		$output2 .= $plans['_rpt_price'];
			    	} else {
				    	$output2 .= '<span class="rpt_currency"></span>' . $plans['_rpt_price'];
			    	}
			    } else {

			    	$currency = get_post_meta( $post->ID, '_rpt_currency', true );

			    	if (!empty($currency)){
			    		$output2 .= '<span class="rpt_currency">';
			    		$output2 .= $currency;
						$output2 .= '</span>';
					}

			    	$output2 .= $plans['_rpt_price'];

			    }

	   		    $output2 .= '</div>';

	   		/* --- Subtitle color if specific skins --- */
	   		if ($skin == 'lottacolor' || $skin == 'bitcolor'){
	   			$subtitle_color = 'white';

	   		} else {
	   		    $subtitle_color = $plans['_rpt_color'];
	   		}
                /* --- Plan subtitle --- */
	   		if (!empty($plans['_rpt_subtitle'])){
	   		    	$output2 .= '<div style="color:' . $subtitle_color . ';" class="rpt_subtitle rpt_subtitle_' . $key . '">' . $plans['_rpt_subtitle'] . '</div>';
	   		    }

	   		/* --- Description --- */
	   		if (!empty($plans['_rpt_description'])){
	   		    $output2 .= '<div class="rpt_description rpt_description_' . $key . '">' . $plans['_rpt_description'] . '</div>';
	   		}

	   	/* --- Closing plan head --- */
	   	$output2 .= '</div>';

	   	/* --- Plan features --- */
	   	if (!empty($plans['_rpt_features'])){

	   		$output2 .= '<div class="rpt_features rpt_features_' . $key . '">';

        /* --- Check for not available feature (in the plans) --- */
	   		$string = $plans['_rpt_features'];
	   		$stringAr = explode("\n", $string);
	   		$stringAr = array_filter($stringAr, 'trim');

	   		$features = '';

	   		foreach ($stringAr as $feature) {
	   			$features[] .= strip_tags($feature,'<strong></strong><br/><br></br><img><a>');
	   		}

	   		foreach ($features as $small_key => $feature){
	   			if (!empty($feature)){

            /* --- Apply grey color if feature is declared as not available --- */
	   				$check = substr($feature, 0, 2);
	   				if ($check == '-n') {
	   					$feature = substr($feature, 2);
	   					$check_color = '#cccccc';
	   				} else {
	   					$check_color = '#555555';
	   				}

					/* --- Plan feature with tooltip --- */
	   				if (strpos($feature,'{') !== false && strpos($feature,'}') !== false) {
	   					preg_match('~{(.*?)}~', $feature, $tooltip_content);
	   					$tooltip_text = str_replace(array('{','}'), '', $tooltip_content[0]);
	   					$feature = preg_replace('/{.*/', '', $feature);
	   					$feature .= '<span style="color:' . $plans['_rpt_color'] . ' !important; text-decoration:none !important; display:inline; font-size:12px; font-weight:bold; position:relative; top:-6px;"> +</span>';

	   					// Re-aligning features if tooltip is added
	   					if ($skin == 'sweetlines'){
	   						$output2 .= '<div style="color:' . $check_color . ';" class="rpt_feature rpt_feature_' . $key . '-' . $small_key . '"><a href="javascript:void(0)" style="position:relative; color:' . $check_color . ';" class="rpt_tooltip">';
	   					} else {
	   						$output2 .= '<div style="color:' . $check_color . ';" class="rpt_feature rpt_feature_' . $key . '-' . $small_key . '"><a href="javascript:void(0)" style="position:relative; color:' . $check_color . ';" class="rpt_tooltip">';
	   					}

	   					$output2 .= '<span class="intool"><b></b>' . $tooltip_text . '</span>';
	   					$output2 .= $feature;
	   					$output2 .= '</a></div>';

	   				} else {
	   				    /* --- Plan feature without tooltip --- */
	   					$output2 .= '<div style="color:' . $check_color . ';" class="rpt_feature rpt_feature_' . $key . '-' . $small_key . '">';
	   					$output2 .= $feature;
	   					$output2 .= '</div>';
	   				}
	   			}
	   		}
	   		/* --- Closing plan features --- */
	   		$output2 .= '</div>';
	   	}

       /* --- Get button link and text or assigning default values --- */
	   	if (!empty($plans['_rpt_btn_text'])){
	   		$btn_text =	$plans['_rpt_btn_text'];
	   		if (!empty($plans['_rpt_btn_link'])){
	   			$btn_link =	$plans['_rpt_btn_link'];
	   		} else { $btn_link = '#'; }
	   	} else {
	   		$btn_text =	'';
	   		$btn_link = '#';
	   	}

	   	/* --- Link option --- */
	   	$newcurrentwindow = get_post_meta( $post->ID, '_rpt_open_newwindow', true );
	   	if ($newcurrentwindow == 'newwindow'){
	   		$link_behavior = 'target="_blank"';
	   	} else {
	   		$link_behavior = 'target="_self"';
	   	}


	   	if (!empty($plans['_rpt_btn_custom_btn'])){
            if ($skin == 'basic'){
                $output2 .= '<div class="rpt_custom_btn" style="border-bottom-left-radius:5px; border-bottom-right-radius:5px; text-align:center; padding:16px 20px; background-color:'.$plans['_rpt_color'].'">';
                    $output2 .= do_shortcode($plans['_rpt_btn_custom_btn']);
                $output2 .= '</div>';
            }
            if ($skin == 'sweetlines'){
                $output2 .= '<div class="rpt_custom_btn" style="text-align:center; padding:16px 20px;">';
                    $output2 .= do_shortcode($plans['_rpt_btn_custom_btn']);
                $output2 .= '</div>';
            }
            if ($skin != 'basic' && $skin != 'sweetlines'){
                $output2 .= '<div class="rpt_custom_btn" style="text-align:center; padding:16px 20px; background-color:'.$plans['_rpt_color'].'">';
                    $output2 .= do_shortcode($plans['_rpt_btn_custom_btn']);
                $output2 .= '</div>';
            }
        } else {
	   	   /* --- Default footer (Plan button) --- */
            if (!empty($plans['_rpt_btn_text'])){

	   	      if ($skin == 'sweetlines'){
	   	      	$output2 .= '<a '. $link_behavior .' href="' . do_shortcode($btn_link) . '" style="background:white; color:' . $plans['_rpt_color'] . '" class="rpt_foot rpt_foot_' . $key . '">';
	   	      } else {
	   	      	$output2 .= '<a '. $link_behavior .' href="' . do_shortcode($btn_link) . '" style="background:' . $plans['_rpt_color'] . '" class="rpt_foot rpt_foot_' . $key . '">';
	   	      }

            } else {

                if ($skin == 'sweetlines'){
	   	      	$output2 .= '<a '. $link_behavior .' style="background:white; color:' . $plans['_rpt_color'] . '" class="rpt_foot rpt_foot_' . $key . '">';
	   	      } else {
	   	      	$output2 .= '<a '. $link_behavior .' style="background:' . $plans['_rpt_color'] . '" class="rpt_foot rpt_foot_' . $key . '">';
	   	      }

            }

	   	   $output2 .= do_shortcode($btn_text);

	   	   /* --- Closing footer --- */
	   	   $output2 .= '</a>';
        }

       /* --- Closing current plan (pricing tables have from 2 to 5 plans) --- */
       $output2 .= '</div>';

	}

	/* --- Closing rpt_inner --- */
	$output2 .= '</div>';

	/* --- Closing rpt_container (the wrapper) --- */
	$output2 .= '</div>';

	$output2 .= '<div style="margin-bottom:24px; clear:both;"></div>';

    endforeach; wp_reset_postdata();
  return $output2;
}?>
