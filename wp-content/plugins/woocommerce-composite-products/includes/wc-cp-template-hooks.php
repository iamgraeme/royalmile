<?php
/**
 * Composite Products Template Hooks.
 *
 * @version  3.2.1
 * @since    3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Single product template hooks
 */

// Single product template
add_action( 'woocommerce_composite_add_to_cart', 'wc_cp_add_to_cart' );

// Single product add-to-cart button template for composite products
add_action( 'woocommerce_composite_add_to_cart_button', 'wc_cp_add_to_cart_button' );

/*
 * Basic layout elements
 *
 * Note:
 *
 * If 'wc_cp_add_paged_mode_cart' is moved to a later priority, the add-to-cart and summary section will no longer be part of the step-based process
 * In this case, use 'wc_cp_add_paged_mode_final_step_scroll_target' to define the auto-scroll target after clicking on the "Next" button of the final component, like so:
 * add_action( 'woocommerce_composite_after_components', 'wc_cp_add_paged_mode_final_step_scroll_target', 9, 2 );
 */

// Before components: scroll target + pagination
add_action( 'woocommerce_composite_before_components', 'wc_cp_add_paged_mode_select_component_option_scroll_target_thumbnails', 5, 2 );
add_action( 'woocommerce_composite_before_components', 'wc_cp_add_paged_mode_show_component_scroll_target', 10, 2 );
add_action( 'woocommerce_composite_before_components', 'wc_cp_add_paged_mode_pagination', 15, 2 );
add_action( 'woocommerce_composite_before_components', 'wc_cp_add_navigation_top', 20, 2 );

// After components: cart, navigation + scroll target
add_action( 'woocommerce_composite_after_components', 'wc_cp_add_single_mode_cart', 10, 2 );
add_action( 'woocommerce_composite_after_components', 'wc_cp_add_paged_mode_cart', 10, 2 );
add_action( 'woocommerce_composite_after_components', 'wc_cp_add_component_messages', 15, 2 );
add_action( 'woocommerce_composite_after_components', 'wc_cp_add_navigation_bottom', 15, 2 );
add_action( 'woocommerce_composite_after_components', 'wc_cp_add_paged_mode_select_component_option_scroll_target_dropdowns', 20, 2 );

/*
 * Stacked layout hooks
 */

// Component options: Sorting and filtering
add_action( 'woocommerce_composite_component_selections_single', 'wc_cp_add_sorting', 10, 2 );
add_action( 'woocommerce_composite_component_selections_single', 'wc_cp_add_filtering', 20, 2 );

// Component options: Dropdowns / Product thumbnails
add_action( 'woocommerce_composite_component_selections_single', 'wc_cp_add_component_options', 25, 2 );

// Component options: Pagination
add_action( 'woocommerce_composite_component_selections_single', 'wc_cp_add_component_options_pagination', 26, 2 );

// Component options: Current selection in single-page mode
add_action( 'woocommerce_composite_component_selections_single', 'wc_cp_add_current_selection_details', 30, 2 );

/*
 * Progressive layout hooks
 */

// Component options: Current selections block wrapper in progressive mode -- start
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_progressive_mode_block_wrapper_start', 5, 2 );

// Component options: Sorting and filtering
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_sorting', 10, 2 );
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_filtering', 20, 2 );

// Component options: Dropdowns / Product thumbnails
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_component_options', 25, 2 );

// Component options: Pagination
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_component_options_pagination', 26, 2 );

// Component options: Current selections block wrapper in progressive mode -- end
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_progressive_mode_block_wrapper_end', 29, 2 );

// Component options: Current selection in single-page mode
add_action( 'woocommerce_composite_component_selections_progressive', 'wc_cp_add_current_selection_details', 35, 2 );

/*
 * Stepped and Componentized layout hooks
 */

// Component options: Current selection details in paged mode - before thumbnails
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_current_selection_details_paged_top', 10, 2 );

// Component options: Sorting and filtering
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_sorting', 15, 2 );
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_filtering', 20, 2 );

// Component options: Dropdowns / Product thumbnails
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_component_options', 25, 2 );

// Component options: Pagination
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_component_options_pagination', 26, 2 );

// Component options: Current selection in paged mode - after dropdown
add_action( 'woocommerce_composite_component_selections_multi', 'wc_cp_add_current_selection_details_paged_bottom', 30, 2 );

// Summary added inside the composite-add-to-cart.php template
add_action( 'woocommerce_before_add_to_cart_button', 'wc_cp_before_add_to_cart_button', 5 );

/*
 * Composited product template hooks
 */

// Composited product title
add_action( 'woocommerce_composite_show_composited_product', 'wc_cp_composited_product_title', 5, 3 );

// Composited product details wrapper open
add_action( 'woocommerce_composite_show_composited_product', 'wc_cp_composited_product_details_wrapper_open', 10, 3 );

// Composited product thumbnail
add_action( 'woocommerce_composite_show_composited_product', 'wc_cp_composited_product_thumbnail', 20, 3 );

// Composited product details
add_action( 'woocommerce_composite_show_composited_product', 'wc_cp_composited_product_details', 30, 3 );

// Composited product details wrapper close
add_action( 'woocommerce_composite_show_composited_product', 'wc_cp_composited_product_details_wrapper_close', 100, 3 );


// Composited product - Simple product template data
add_action( 'woocommerce_composite_show_composited_product_simple', 'wc_cp_composited_product_details_simple', 10, 3 );

// Composited product - Variable product template data
add_action( 'woocommerce_composite_show_composited_product_variable', 'wc_cp_composited_product_details_variable', 10, 3 );

// Composited product excerpt
add_action( 'woocommerce_composited_product_details', 'wc_cp_composited_product_excerpt', 10, 3 );

// Composited Simple product price
add_action( 'woocommerce_composited_product_add_to_cart', 'wc_cp_composited_product_price', 8, 3 );
