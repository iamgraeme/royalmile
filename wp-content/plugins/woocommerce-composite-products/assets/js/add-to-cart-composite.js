/* jshint -W069 */
/* jshint -W041 */
/* jshint -W018 */

/**
 * Composite scripts, accessible to the outside world
 */
var wc_cp_composite_scripts = {};

jQuery( document ).ready( function($) {

	$( 'body' ).on( 'quick-view-displayed', function() {

		$( '.composite_form .composite_data' ).each( function() {
			$(this).wc_composite_form();
		} );
	} );

	/**
	 * Responsive form CSS (we can't rely on media queries since we must work with the .composite_form width, not screen width)
	 */
	$( window ).resize( function() {

		$( '.composite_form' ).each( function() {

			if ( $(this).width() <= wc_composite_params.small_width_threshold ) {
				$(this).addClass( 'small_width' );
			} else {
				$(this).removeClass( 'small_width' );
			}

			if ( $(this).width() > wc_composite_params.full_width_threshold ) {
				$(this).addClass( 'full_width' );
			} else {
				$(this).removeClass( 'full_width' );
			}

			if ( wc_composite_params.legacy_width_threshold ) {
				if ( $(this).width() <= wc_composite_params.legacy_width_threshold ) {
					$(this).addClass( 'legacy_width' );
				} else {
					$(this).removeClass( 'legacy_width' );
				}
			}
		} );

	} ).trigger( 'resize' );

	/**
	 * BlockUI background params
	 */
	var wc_cp_block_params = {};

	if ( wc_composite_params.is_wc_version_gte_2_3 === 'yes' ) {

		wc_cp_block_params = {
			message:    null,
			overlayCSS: {
				background: 'rgba( 255, 255, 255, 0 )',
				opacity:    0.6
			}
		};
	} else {

		wc_cp_block_params = {
			message:    null,
			overlayCSS: {
				background: 'rgba( 255, 255, 255, 0 ) url(' + woocommerce_params.ajax_loader_url + ') no-repeat center',
				backgroundSize: '20px 20px',
				opacity:    0.6
			}
		};
	}

	/**
 	* Populate composite scripts
 	*/
	$.fn.wc_composite_form = function() {

		if ( ! $(this).hasClass( 'composite_data' ) ) {
			return true;
		}

		var composite_data = $(this);
		var container_id   = $(this).data( 'container_id' );

		if ( typeof( wc_cp_composite_scripts[ container_id ] ) !== 'undefined' ) {
			return true;
		}

		var composite_form          = composite_data.closest( '.composite_form' );
		var composite_form_settings = composite_data.data( 'composite_settings' );

		wc_cp_composite_scripts[ container_id ] = {

			$composite_data:                    composite_data,
			$composite_form:                    composite_form,
			$add_to_cart_button:                composite_form.find( '.composite_add_to_cart_button' ),
			$composite_navigation:              composite_form.find( '.composite_navigation' ),
			$composite_navigation_top:          composite_form.find( '.composite_navigation.top' ),
			$composite_pagination:              composite_form.find( '.composite_pagination' ),
			$composite_summary:                 composite_form.find( '.composite_summary' ),
			$composite_summary_widget:          $( '.widget_composite_summary' ),
			$components:                        composite_form.find( '.component' ),

			$composite_price:                   composite_data.find( '.composite_price' ),
			$composite_message:                 composite_data.find( '.composite_message' ),
			$composite_message_content:         composite_data.find( '.composite_message .msg' ),
			$component_message:                 composite_data.find( '.component_message' ),
			$component_message_content:         composite_data.find( '.component_message .msg' ),
			$composite_button:                  composite_data.find( '.composite_button' ),

			$composite_stock_status:            false,

			ajax_url:                           '',

			composite_id:                       container_id,
			composite_settings:                 composite_data.data( 'composite_settings' ),
			composite_layout:                   composite_form_settings.layout,
			composite_layout_variation:         composite_form_settings.layout_variation,
			composite_selection_mode:           composite_form_settings.selection_mode,
			composite_button_behaviour:         composite_form_settings.button_behaviour,
			composite_sequential_comp_progress: composite_form_settings.sequential_componentized_progress,
			actions_nesting:                    0,
			last_active_step_index:             0,
			append_results_nesting:             0,
			append_results_nesting_count:       0,

			composite_components:               {},

			composite_steps:                    {},

			composite_initialized:              false,
			has_update_lock:                    false,
			has_ui_update_lock:                 false,
			has_transition_lock:                false,
			has_update_nav_delay:               false,
			has_scenarios_update_lock:          false,
			active_scenarios:                   {},

			composite_summary_widget:           {},

			/**
			 * Insertion point
			 */

			init: function() {

				/**
				 * Bind composite event handlers
				 */

				this.bind_event_handlers();

				/**
				 * Init components
				 */

				this.init_components();

				/**
				 * Init steps
				 */

				this.init_steps();

				/**
				 * Init widget
				 */

				this.init_widget();

				/**
				 * Initial states and loading
				 */

				var composite      = this;
				var composite_data = composite.$composite_data;

				if ( wc_composite_params.use_wc_ajax === 'yes' ) {
					composite.ajax_url = woocommerce_params.wc_ajax_url;
				} else {
					composite.ajax_url = woocommerce_params.ajax_url;
				}

				if ( typeof( composite.composite_sequential_comp_progress ) === 'undefined' ) {
					composite.composite_sequential_comp_progress = 'yes';
				}

				// Save composite stock status
				if ( composite_data.find( '.composite_wrap p.stock' ).length > 0 ) {
					composite.$composite_stock_status = composite_data.find( '.composite_wrap p.stock' ).clone();
				}

				// Ensure composite msg div exists (template back-compat)
				if ( composite.$composite_message.length == 0 ) {
					composite.$composite_price.after( '<div class="composite_message" style="display:none"><div class="msg woocommerce-error"></div></div>' );
					composite.$composite_message         = composite_data.find( '.composite_message' );
					composite.$composite_message_content = composite.$composite_message.find( '.msg' );
				}
				// Treat as info in paged layouts
				if ( composite.composite_layout === 'paged' ) {
					composite.$composite_message_content.removeClass( 'woocommerce-error' ).addClass( 'woocommerce-info' );
				}

				// Ensure component error div exists
				if ( composite.$composite_navigation.length > 0 ) {
					composite.$component_message         = composite.$composite_form.find( '.component_message' );
					composite.$component_message_content = composite.$component_message.find( '.msg' );
				}

				// Add-ons support - move totals container
				var addons_totals = composite_data.find( '#product-addons-total' );
				composite.$composite_price.after( addons_totals );

				// NYP support
				composite_data.find( '.nyp' ).trigger( 'woocommerce-nyp-updated-item' );

				// Init toggle boxes
				if ( composite.composite_layout === 'progressive' && ! composite.$composite_data.hasClass( 'composite_added_to_cart' ) ) {
					composite.$composite_form.find( '.toggled:not(.active) .component_title' ).addClass( 'inactive' );
				}

				// Trigger pre-init event
				composite_data.trigger( 'wc-composite-initializing' );


				// Initialize component selection states and quantities for all modes

				composite.has_scenarios_update_lock = true;

				$.each( composite.composite_components, function( index, component ) {

					var component_options_select = component.$component_options.find( 'select.component_options_select' );
					component.set_selection_id( component_options_select.val() );

					// Load main component scripts
					component.init_scripts();

					// Load 3rd party scripts
					component.$self.trigger( 'wc-composite-component-loaded' );

				} );

				composite.has_scenarios_update_lock = false;

				// Make the form visible
				composite.$composite_form.css( 'visibility', 'visible' );

				// Activate initial step
				var current_step = composite.get_current_step();

				composite.last_active_step_index = current_step.step_index;

				if ( composite.composite_layout === 'paged' || composite.composite_layout === 'progressive' ) {

					current_step.show_step();

				} else {

					current_step.set_active();

					current_step.fire_scenario_actions();

					composite.update_ui();
				}

				// Set the form as initialized and validate/update it
				composite.composite_initialized = true;

				composite.update_composite();

				// Let 3rd party scripts know that all component options are loaded
				this.$components.each( function() {
					$(this).trigger( 'wc-composite-component-options-loaded' );
				} );

				// Trigger post-init event
				composite_data.trigger( 'wc-composite-initialized' );

			},

			/**
			 * Attach composite-level event handlers
			 */

			bind_event_handlers: function() {

				var composite = this;

				/**
				 * Change the post names of variation/attribute fields to make them unique (for debugging purposes)
				 * Data from these fields is copied and posted in new unique vars - see below
				 * To maintain variations script compatibility with WC 2.2, we can't use our own unique field names in the variable-product.php template
				 */
				this.$add_to_cart_button

					.on( 'click', function() {

						$.each( composite.composite_components, function( index, component ) {

							var component_id = component.component_id;

							component.$self.find( '.variations_form .variations .attribute-options select, .variations_form .component_wrap input[name="variation_id"]' ).each( function() {

								$(this).attr( 'name', $(this).attr( 'name' ) + '_' + component_id );
							} );

							component.$self.find( 'select, input' ).each( function() {

								$(this).prop( 'disabled', false );
							} );
						} );
					} );


				/**
				 * Update composite totals when a new NYP price is entered at composite level
				 */
				this.$composite_data

					.on( 'woocommerce-nyp-updated-item', function() {

						var nyp = $(this).find( '.nyp' );

						if ( nyp.length > 0 ) {

							var price_data = $(this).data( 'price_data' );

							price_data[ 'base_price' ]      = nyp.data( 'price' );
							price_data[ 'price_undefined' ] = false;

							composite.update_composite();
						}
					} );


				/**
				 * On clicking the Next / Previous navigation buttons
				 */
				this.$composite_navigation

					.on( 'click', '.page_button', function() {

						if ( $(this).hasClass( 'inactive' ) ) {
							return false;
						}

						if ( composite.has_transition_lock ) {
							return false;
						}

						if ( $(this).hasClass( 'next' ) ) {

							if ( composite.get_next_step() ) {

								composite.show_next_step();

							} else {

								wc_cp_scroll_viewport( composite.$composite_form.find( '.scroll_final_step' ), { partial: false, duration: 250, queue: false } );
							}

						} else {

							composite.show_previous_step();
						}

						$(this).blur();

						return false;

					} );


				/**
				 * On clicking a composite pagination link
				 */
				this.$composite_pagination

					.on( 'click', '.pagination_element a', function() {

						if ( composite.has_transition_lock ) {
							return false;
						}

						if ( $(this).hasClass( 'inactive' ) ) {
							return false;
						}

						var step_id = $(this).closest( '.pagination_element' ).data( 'item_id' );

						composite.show_step( step_id );

						$(this).blur();

						return false;

					} );


				/**
				 * On clicking a composite summary link (review section)
				 */
				this.$composite_summary

					.on( 'click', '.summary_element_link', function() {

						if ( composite.has_transition_lock ) {
							return false;
						}

						var form = composite.$composite_form;

						if ( $(this).hasClass( 'disabled' ) ) {
							return false;
						}

						var step_id = $(this).closest( '.summary_element' ).data( 'item_id' );

						if ( typeof( step_id ) === 'undefined' ) {
							var composite_summary = composite.$composite_summary;
							var element_index     = composite_summary.find( '.summary_element' ).index( $(this).closest( '.summary_element' ) );
							step_id               = form.find( '.multistep.component:eq(' + element_index + ')' ).data( 'item_id' );
						}

						var step = composite.get_step( step_id );

						if ( step === false ) {
							return false;
						}

						if ( step.get_element().hasClass( 'progressive' ) ) {
							step.block_next_steps();
						}

						if ( ! step.is_current() || composite.composite_layout === 'single' ) {
							step.show_step();
						}

						return false;

					} )

					.on( 'click', 'a.summary_element_tap', function() {
						$(this).closest( '.summary_element_link' ).trigger( 'click' );
						return false;
					} );

			},

			/**
			 * Initialize component objects
			 */

			init_components: function() {

				var composite = this;

				composite.$components.each( function( index ) {

					composite.composite_components[ index ] = new WC_CP_Component( composite, $(this), index );

					composite.bind_component_event_handlers( composite.composite_components[ index ] );

				} );

			},

			/**
			 * Attach component-level event handlers
			 */

			bind_component_event_handlers: function( component ) {

				var composite = this;

				component.$self

					.on( 'wc-composite-component-loaded', function() {

						if ( $.isFunction( $.fn.prettyPhoto ) ) {

							$(this).find( 'a[data-rel^="prettyPhoto"]' ).prettyPhoto( {
								hook: 'data-rel',
								social_tools: false,
								theme: 'pp_woocommerce',
								horizontal_padding: 20,
								opacity: 0.8,
								deeplinking: false
							} );
						}
					} )

					/**
					 * Update composite totals when a new Add-on is selected
					 */
					.on( 'woocommerce-product-addons-update', function() {

						var addons = $(this).find( '.addon' );

						if ( addons.length == 0 ) {
							return false;
						}

						composite.update_composite();
					} )

					/**
					 * Update composite totals when a new NYP price is entered
					 */
					.on( 'woocommerce-nyp-updated-item', function() {

						var nyp = $(this).find( '.cart .nyp' );

						if ( nyp.length > 0 && component.get_selected_product_type() !== 'variable' ) {

							component.$component_data.data( 'price', nyp.data( 'price' ) );
							component.$component_data.data( 'regular_price', nyp.data( 'price' ) );

							composite.update_composite();
						}
					} )

					/**
					 * Reset composite totals and form inputs when a new variation selection is initiated
					 */
					.on( 'woocommerce_variation_select_change', function( event ) {

						var summary = component.$component_summary;

						// Reset submit form data - TODO: get rid of this by making the variations script usable regardless of the variations form input names (https://github.com/woothemes/woocommerce/pull/6531)
						composite.$composite_button.find( '.form_data_' + component.component_id + ' .variation_input' ).remove();
						composite.$composite_button.find( '.form_data_' + component.component_id + ' .attribute_input' ).remove();

						// Mark component as not set
						component.$component_data.data( 'component_set', false );

						// Add images class to composited_product_images div ( required by the variations script to flip images )
						summary.find( '.composited_product_images' ).addClass( 'images' );

						$(this).find( '.variations .attribute-options select' ).each( function() {

							if ( $(this).val() === '' ) {
								summary.find( '.component_wrap .single_variation input[name="variation_id"]' ).val( '' );
								summary.find( '.component_wrap .stock' ).addClass( 'inactive' );

								var step = component.get_step();

								step.fire_scenario_actions();

								composite.update_ui();
								composite.update_composite();
								return false;
							}
						} );
					} )

					.on( 'woocommerce_variation_select_focusin', function( event ) {

						component.get_step().fire_scenario_actions( true );
					} )

					.on( 'reset_image', function( event ) {

						var summary = component.$component_summary;

						// Remove images class from composited_product_images div in order to avoid styling issues
						summary.find( '.composited_product_images' ).removeClass( 'images' );
					} )

					/**
					 * Update composite totals and form inputs when a new variation is selected
					 */
					.on( 'found_variation', function( event, variation ) {

						var summary   = component.$component_summary;
						var form_data = composite.$composite_button.find( '.form_data_' + component.component_id );

						// Start copying submit form data - TODO: get rid of this by making the variations script usable regardless of the variations form input names (https://github.com/woothemes/woocommerce/pull/6531)
						var variation_data 	= '<input type="hidden" name="wccp_variation_id[' + component.component_id + ']" class="variation_input" value="' + variation.variation_id + '"/>';
						form_data.append( variation_data );

						for ( var attribute in variation.attributes ) {
							var attribute_data 	= '<input type="hidden" name="wccp_' + attribute + '[' + component.component_id + ']" class="attribute_input" value="' + $(this).find( '.variations .attribute-options select[name="' + attribute + '"]' ).val() + '"/>';
							form_data.append( attribute_data );
						}
						// End copying form data

						// Copy variation price data
						var price_data = composite.$composite_data.data( 'price_data' );

						if ( price_data[ 'per_product_pricing' ] == true ) {
							component.$component_data.data( 'price', variation.price );
							component.$component_data.data( 'regular_price', variation.regular_price );
						}

						// Mark component as set
						component.$component_data.data( 'component_set', true );

						// Remove images class from composited_product_images div in order to avoid styling issues
						summary.find( '.composited_product_images' ).removeClass( 'images' );

						// Handle sold_individually variations qty
						if ( variation.is_sold_individually === 'yes' ) {
							$(this).find( '.component_wrap input.qty' ).val( '1' ).change();
						}

						component.get_step().fire_scenario_actions();

						composite.update_ui();
						composite.update_composite();
					} )

					/**
					 * Event triggered by custom product types to indicate that the state of the component selection has changed
					 */
					.on ( 'woocommerce-composited-product-update', function( event ) {

						var price_data = composite.$composite_data.data( 'price_data' );

						if ( price_data[ 'per_product_pricing' ] == true ) {

							var bundle_price         = component.$component_data.data( 'price' );
							var bundle_regular_price = component.$component_data.data( 'regular_price' );

							component.$component_data.data( 'price', bundle_price );
							component.$component_data.data( 'regular_price', bundle_regular_price );
						}

						composite.update_ui();
						composite.update_composite();
					} )

					/**
					 * On clicking the clear options button
					 */
					.on( 'click', '.clear_component_options', function( event ) {

						if ( $(this).hasClass( 'reset_component_options' ) ) {
							return false;
						}

						var selection = component.$component_options.find( 'select.component_options_select' );

						component.get_step().unblock_step_inputs();

						component.$self.find( '.component_option_thumbnails .selected' ).removeClass( 'selected' );

						selection.val( '' ).change();

						return false;
					} )

					/**
					 * On clicking the reset options button
					 */
					.on( 'click', '.reset_component_options', function( event ) {

						var step      = component.get_step();
						var selection = component.$component_options.find( 'select.component_options_select' );

						step.unblock_step_inputs();

						component.$self.find( '.component_option_thumbnails .selected' ).removeClass( 'selected' );

						step.set_active();

						selection.val( '' ).change();

						step.block_next_steps();

						return false;
					} )

					/**
					 * On clicking the blocked area in progressive mode
					 */
					.on( 'click', '.block_component_selections_inner', function( event ) {

						var step = component.get_step();

						step.block_next_steps();
						step.show_step();

						return false;
					} )

					/**
					 * On clicking a thumbnail
					 */
					.on( 'click', '.component_option_thumbnail', function( event ) {

						var item = component.$self;

						if ( item.hasClass( 'disabled' ) || $(this).hasClass( 'disabled' ) ) {
							return true;
						}

						$(this).blur();

						if ( ! $(this).hasClass( 'selected' ) ) {
							var value = $(this).data( 'val' );
							component.$component_options.find( 'select.component_options_select' ).val( value ).change();
						}

					} )

					.on( 'click', 'a.component_option_thumbnail_tap', function( event ) {
						$(this).closest( '.component_option_thumbnail' ).trigger( 'click' );
						return false;
					} )

					.on( 'focusin touchstart', '.component_options select.component_options_select', function( event ) {

						component.get_step().fire_scenario_actions( true );

					} )

					/**
					 * On changing a component option
					 */
					.on( 'change', '.component_options select.component_options_select', function( event ) {

						var component_content    = component.$component_content;
						var summary_content      = component.$self.find( '.component_summary > .content' );
						var form_data            = composite.$composite_button.find( '.form_data_' + component.component_id );
						var selected_product_id  = $(this).val();

						$(this).blur();

						// Reset submit data
						form_data.find( '.variation_input' ).remove();
						form_data.find( '.attribute_input' ).remove();

						// Select thumbnail
						component.$self.find( '.component_option_thumbnails .selected' ).removeClass( 'selected disabled' );
						component.$self.find( '#component_option_thumbnail_' + $(this).val() ).addClass( 'selected' );

						var data = {
							action: 		'woocommerce_show_composited_product',
							product_id: 	selected_product_id,
							component_id: 	component.component_id,
							composite_id: 	composite.composite_id,
							security: 		wc_composite_params.show_product_nonce
						};

						// Remove all event listeners
						summary_content.removeClass( 'variations_form bundle_form cart' );
						component_content.off().find( '*' ).off();

						component.select_component_option( data );

						// Finito
						return false;
					} )

					/**
					 * Append component options upon clicking the "Load more" button
					 */
					.on( 'click', '.component_pagination a.component_options_load_more', function( event ) {

						var item                    = component.$self;
						var item_id                 = component.component_id;
						var component_ordering      = item.find( '.component_ordering select' );

						// Variables to post
						var page                    = parseInt( $(this).data( 'pages_loaded' ) ) + 1;
						var pages                   = parseInt( $(this).data( 'pages' ) );
						var selected_option         = component.get_selected_product_id();
						var container_id            = composite.composite_id;
						var filters                 = component.get_active_filters();

						if ( page > pages ) {
							return false;
						}

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			page,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							filters:            filters,
							security: 			wc_composite_params.show_product_nonce
						};

						// Current 'orderby' setting
						if ( component_ordering.length > 0 ) {
							data.orderby = component_ordering.val();
						}

						// Update component options
						if ( data.load_page > 0 ) {
							$(this).blur();
							component.append_component_options( data );
						}

						// Finito
						return false;

					} )

					/**
					 * Refresh component options upon clicking on a component options page
					 */
					.on( 'click', '.component_pagination a.component_pagination_element', function( event ) {

						var item                    = component.$self;
						var item_id                 = component.component_id;
						var component_ordering      = item.find( '.component_ordering select' );

						// Variables to post
						var page                    = parseInt( $(this).data( 'page_num' ) );
						var selected_option         = component.get_selected_product_id();
						var container_id            = composite.composite_id;
						var filters                 = component.get_active_filters();

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			page,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							filters:            filters,
							security: 			wc_composite_params.show_product_nonce
						};

						// Current 'orderby' setting
						if ( component_ordering.length > 0 ) {
							data.orderby = component_ordering.val();
						}

						// Update component options
						if ( data.load_page > 0 ) {
							$(this).blur();
							component.reload_component_options( data );
						}

						// Finito
						return false;

					} )

					/**
					 * Refresh component options upon reordering
					 */
					.on( 'change', '.component_ordering select', function( event ) {

						var item_id                 = component.component_id;

						// Variables to post
						var selected_option         = component.get_selected_product_id();
						var container_id            = composite.composite_id;
						var orderby                 = $(this).val();
						var filters                 = component.get_active_filters();

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			1,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							orderby: 			orderby,
							filters:            filters,
							security: 			wc_composite_params.show_product_nonce
						};

						$(this).blur();

						// Update component options
						component.reload_component_options( data );

						// Finito
						return false;

					} )

					/**
					 * Refresh component options upon activating a filter
					 */
					.on( 'click', '.component_filter_option a', function( event ) {

						var item                    = component.$self;
						var item_id                 = component.component_id;
						var component_ordering      = item.find( '.component_ordering select' );

						var component_filter_option = $(this).closest( '.component_filter_option' );

						// Variables to post
						var selected_option         = component.get_selected_product_id() > 0 ? component.get_selected_product_id() : '';
						var container_id            = composite.composite_id;
						var filters                 = {};

						if ( ! component_filter_option.hasClass( 'selected' ) ) {
							component_filter_option.addClass( 'selected' );
						} else {
							component_filter_option.removeClass( 'selected' );
						}

						// add / remove 'active' classes
						component.update_filters_ui();

						// get active filters
						filters = component.get_active_filters();

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			1,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							filters: 			filters,
							security: 			wc_composite_params.show_product_nonce
						};

						// Current 'orderby' setting
						if ( component_ordering.length > 0 ) {
							data.orderby = component_ordering.val();
						}

						$(this).blur();

						// Update component options
						component.reload_component_options( data );

						// Finito
						return false;

					} )

					/**
					 * Refresh component options upon resetting all filters
					 */
					.on( 'click', '.component_filters a.reset_component_filters', function( event ) {

						var item                    = component.$self;
						var item_id                 = component.component_id;
						var component_ordering      = item.find( '.component_ordering select' );

						// Get active filters
						var component_filter_options = item.find( '.component_filters .component_filter_option.selected' );

						if ( component_filter_options.length == 0 ) {
							return false;
						}

						// Variables to post
						var selected_option         = component.get_selected_product_id() > 0 ? component.get_selected_product_id() : '';
						var container_id            = composite.composite_id;
						var filters                 = {};

						component_filter_options.removeClass( 'selected' );

						// add / remove 'active' classes
						component.update_filters_ui();

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			1,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							filters: 			filters,
							security: 			wc_composite_params.show_product_nonce
						};

						// Current 'orderby' setting
						if ( component_ordering.length > 0 ) {
							data.orderby = component_ordering.val();
						}

						$(this).blur();

						// Update component options
						component.reload_component_options( data );

						// Finito
						return false;

					} )

					/**
					 * Refresh component options upon resetting a filter
					 */
					.on( 'click', '.component_filters a.reset_component_filter', function( event ) {

						var item                     = component.$self;
						var item_id                  = component.component_id;
						var component_ordering       = item.find( '.component_ordering select' );

						// Get active filters
						var component_filter_options = $(this).closest( '.component_filter' ).find( '.component_filter_option.selected' );

						if ( component_filter_options.length == 0 ) {
							return false;
						}

						// Variables to post
						var selected_option         = component.get_selected_product_id() > 0 ? component.get_selected_product_id() : '';
						var container_id            = composite.composite_id;
						var filters                 = {};

						component_filter_options.removeClass( 'selected' );

						// add / remove 'active' classes
						component.update_filters_ui();

						// get active filters
						filters = component.get_active_filters();

						var data = {
							action: 			'woocommerce_show_component_options',
							load_page: 			1,
							component_id: 		item_id,
							composite_id: 		container_id,
							selected_option: 	selected_option,
							filters: 			filters,
							security: 			wc_composite_params.show_product_nonce
						};

						// Current 'orderby' setting
						if ( component_ordering.length > 0 ) {
							data.orderby = component_ordering.val();
						}

						$(this).blur();

						// Update component options
						component.reload_component_options( data );

						// Finito
						return false;

					} )

					/**
					 * Expand / Collapse filters
					 */
					.on( 'click', '.component_filter_title label', function( event ) {

						var component_filter         = $(this).closest( '.component_filter' );
						var component_filter_content = component_filter.find( '.component_filter_content' );

						wc_cp_toggle_element( component_filter, component_filter_content );

						$(this).blur();

						// Finito
						return false;

					} )

					/**
					 * Expand / Collapse components
					 */
					.on( 'click', '.component_title', function( event ) {

						var item = component.$self;
						var form = composite.$composite_form;

						if ( ! item.hasClass( 'toggled' ) || $(this).hasClass( 'inactive' ) ) {
							return false;
						}

						if ( item.hasClass( 'progressive' ) && item.hasClass( 'active' ) ) {
							return false;
						}

						var component_inner = component.$component_inner;

						if ( wc_cp_toggle_element( item, component_inner ) ) {

							if ( item.hasClass( 'progressive' ) ) {

								if ( item.hasClass( 'blocked' ) ) {

									form.find( '.page_button.next' ).click();

								} else if ( item.hasClass( 'disabled' ) ) {

									var step = component.get_step();

									step.block_next_steps();
									step.show_step();
								}
							}
						}

						$(this).blur();

						// Finito
						return false;

					} )

					/**
					 * Update composite totals upon changing quantities
					 */
					.on( 'change', '.component_wrap input.qty', function( event ) {

						var min = parseFloat( $(this).attr( 'min' ) );
						var max = parseFloat( $(this).attr( 'max' ) );

						if ( min >= 0 && parseFloat( $(this).val() ) < min ) {
							$(this).val( min );
						}

						if ( max > 0 && parseFloat( $(this).val() ) > max ) {
							$(this).val( max );
						}

						composite.update_composite();
					} );

			},

			/**
			 * Initialize composite step objects
			 */

			init_steps: function() {

				var composite = this;

				/*
				 * Prepare markup for "Review" step, if needed
				 */

				if ( composite.composite_layout === 'paged' ) {

					// Componentized layout: replace the step-based process with a summary-based process
					if ( composite.composite_layout_variation === 'componentized' ) {

						composite.$composite_form.find( '.multistep.active' ).removeClass( 'active' );
						composite.$composite_data.addClass( 'multistep active' );

						// No review step in the pagination template
						composite.$composite_pagination.find( '.pagination_element_review' ).remove();

						// No summary widget
						composite.$composite_summary_widget.hide();

					// If the composite-add-to-cart.php template is added right after the component divs, it will be used as the final step of the step-based configuration process
					} else if ( composite.$composite_data.prev().hasClass( 'multistep' ) ) {

						composite.$composite_data.addClass( 'multistep' );
						composite.$composite_data.hide();

						// If the composite was just added to the cart, make the review/summary step active
						if ( composite.$composite_data.hasClass( 'composite_added_to_cart' ) ) {
							composite.$composite_form.find( '.multistep.active' ).removeClass( 'active' );
							composite.$composite_data.addClass( 'active' );
						}

					} else {

						composite.$composite_pagination.find( '.pagination_element_review' ).remove();
					}

				} else if ( composite.composite_layout === 'progressive' ) {

					// If the composite was just added to the cart, make the last step active
					if ( composite.$composite_data.hasClass( 'composite_added_to_cart' ) ) {
						composite.$components.removeClass( 'blocked open' ).addClass( 'closed disabled' );
						composite.$components.filter( '.toggled' ).find( '.component_inner' ).hide();
						composite.$components.filter( '.toggled' ).find( '.component_title' ).removeClass( 'inactive' ).addClass( 'active' );
						composite.$components.filter( '.multistep.active' ).removeClass( 'active' );
						composite.$components.filter( '.multistep.last' ).addClass( 'active' );
					}
				}

				/*
				 * Initialize steps
				 */

				composite.$composite_form.children( '.component, .multistep' ).each( function( index ) {

					composite.composite_steps[ index ] = new WC_CP_Step( composite, $(this), index );

				} );

			},

			/**
			 * Shows a step when its id is known
			 */

			show_step: function( step_id ) {

				var composite = this;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.step_id == step_id ) {
						step.show_step();
						return false;
					}

				} );

			},

			/**
			 * Shows the step marked as previous from the current one
			 */

			show_previous_step: function() {

				var composite = this;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.is_previous() ) {
						step.show_step();
						return false;
					}

				} );

			},

			/**
			 * Shows the step marked as next from the current one
			 */

			show_next_step: function() {

				var composite = this;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.is_next() ) {
						step.show_step();
						return false;
					}

				} );

			},

			/**
			 * Returns a step object by id
			 */

			get_step: function( step_id ) {

				var composite = this;
				var found     = false;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.step_id == step_id ) {
						found = step;
						return false;
					}

				} );

				return found;

			},

			/**
			 * Returns the current step object
			 */

			get_current_step: function() {

				var composite = this;
				var current   = false;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.is_current() ) {
						current = step;
						return false;
					}

				} );

				return current;

			},

			/**
			 * Returns the previous step object
			 */

			get_previous_step: function() {

				var composite  = this;
				var previous   = false;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.is_previous() ) {
						previous = step;
						return false;
					}

				} );

				return previous;

			},

			/**
			 * Returns the next step object
			 */

			get_next_step: function() {

				var composite = this;
				var next      = false;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step.is_next() ) {
						next = step;
						return false;
					}

				} );

				return next;

			},

			/**
			 * Return stored scenario data
			 */

			get_scenario_data: function() {

				var composite = this;
				var scenarios = composite.$composite_data.data( 'scenario_data' );

				return scenarios;

			},

			/**
			 * Extract active scenarios from current selections
			 */

			update_active_scenarios: function( firing_step_id, return_result ) {

				var composite         = this;
				var style             = composite.composite_layout;

				var fired_by_step;

				if ( style !== 'single' ) {
					fired_by_step  = composite.get_current_step();
					firing_step_id = fired_by_step.step_id;
				} else {
					fired_by_step = composite.get_step( firing_step_id );
				}

				var firing_step_index = fired_by_step.step_index;
				var tabs              = '';

				for ( var i = composite.actions_nesting - 1; i > 0; i-- ) {
					tabs = tabs + '	';
				}

				if ( typeof( return_result ) === 'undefined' ) {
					return_result = false;
				}

				if ( fired_by_step.is_review() ) {
					firing_step_index = 1000;
				}

				// Initialize by getting all scenarios
				var scenarios = composite.get_scenario_data().scenarios;

				var compat_group_scenarios = composite.get_scenarios_by_type( scenarios, 'compat_group' );

				if ( compat_group_scenarios.length === 0 ) {
					scenarios.push( '0' );
				}

				// Active scenarios including current component
				var active_scenarios_incl_current            = scenarios;

				// Active scenarios excluding current component
				var active_scenarios_excl_current            = scenarios;

				var scenario_shaping_components_incl_current = [];
				var scenario_shaping_components_excl_current = [];

				if ( wc_composite_params.script_debug === 'yes' ) {

					if ( return_result ) {
						tabs = tabs + '	';
						wc_cp_log( '\n' + tabs + 'Scenarios requested by ' + fired_by_step.get_title() + ' at ' + new Date().getTime().toString() + '...' );
					} else {
						wc_cp_log( '\n' + tabs + 'Selections update triggered by ' + fired_by_step.get_title() + ' at ' + new Date().getTime().toString() + '...' );
					}

					wc_cp_log( '\n' + tabs + 'Calculating active scenarios...' );
				}

				// Incl current
				$.each( composite.composite_components, function( index, component ) {

					var component_id = component.component_id;

					if ( style === 'progressive' || style === 'paged' ) {

						if ( index > firing_step_index ) {
							return false;
						}

						active_scenarios_excl_current            = active_scenarios_incl_current.slice();
						scenario_shaping_components_excl_current = scenario_shaping_components_incl_current.slice();

					}

					var product_id   = component.get_selected_product_id();
					var product_type = component.get_selected_product_type();

					if ( product_id !== null && product_id >= 0 ) {

						var scenario_data      = composite.get_scenario_data().scenario_data;
						var item_scenario_data = scenario_data[ component_id ];

						// Treat '' optional component selections as 'None' if the component is optional
						if ( product_id === '' ) {
							if ( 0 in item_scenario_data ) {
								product_id = '0';
							} else {
								return true;
							}
						}

						var product_in_scenarios = item_scenario_data[ product_id ];

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + 'Selection #' + product_id + ' of ' + component.get_title() + ' in scenarios: ' + product_in_scenarios.toString() );
						}

						var product_intersection    = wc_cp_intersect_safe( active_scenarios_incl_current, product_in_scenarios );
						var product_is_compatible   = product_intersection.length > 0;

						var variation_is_compatible = true;

						if ( product_is_compatible ) {

							if ( product_type === 'variable' ) {

								var variation_id = component.$self.find( '.single_variation_wrap .variations_button input[name="variation_id"]' ).val();

								if ( variation_id > 0 ) {

									var variation_in_scenarios = item_scenario_data[ variation_id ];

									if ( wc_composite_params.script_debug === 'yes' ) {
										wc_cp_log( tabs + 'Variation selection #' + variation_id + ' of ' + component.get_title() + ' in scenarios: ' + variation_in_scenarios.toString() );
									}

									product_intersection    = wc_cp_intersect_safe( product_intersection, variation_in_scenarios );
									variation_is_compatible = product_intersection.length > 0;
								}
							}
						}

						var is_compatible = product_is_compatible && variation_is_compatible;

						if ( is_compatible ) {

							scenario_shaping_components_incl_current.push( component_id );
							active_scenarios_incl_current = product_intersection;

							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '	Active scenarios: ' + active_scenarios_incl_current.toString() );
							}

						} else {

							// The chosen product was found incompatible
							if ( ! product_is_compatible ) {

								if ( wc_composite_params.script_debug === 'yes' ) {
									wc_cp_log( tabs + '	Selection not found in any scenario - breaking out...' );
								}

								if ( product_id !== '0' && ( style === 'single' || firing_step_id == component_id ) ) {
									component.$self.addClass( 'reset' );
								}

							} else {

								if ( wc_composite_params.script_debug === 'yes' ) {
									wc_cp_log( tabs + '	Variation selection not found in any scenario - breaking out and resetting...' );
								}

								if ( style === 'single' || firing_step_id == component_id ) {
									component.$self.addClass( 'reset_variation' );
								}
							}
						}
					}

				} );

				if ( style === 'single' ) {

					// Excl current
					$.each( composite.composite_components, function( index, component ) {

						var component_id = component.component_id;

						if ( index == firing_step_index || component.$self.hasClass( 'reset' ) || component.$self.hasClass( 'reset_variation' ) ) {
							return true;
						}

						var product_id   = component.get_selected_product_id();
						var product_type = component.get_selected_product_type();

						if ( product_id !== null && product_id >= 0 ) {

							var scenario_data      = composite.get_scenario_data().scenario_data;
							var item_scenario_data = scenario_data[ component_id ];

							// Treat '' optional component selections as 'None' if the component is optional
							if ( product_id === '' ) {
								if ( 0 in item_scenario_data ) {
									product_id = '0';
								} else {
									return true;
								}
							}

							var product_in_scenarios    = item_scenario_data[ product_id ];
							var product_intersection    = wc_cp_intersect_safe( active_scenarios_excl_current, product_in_scenarios );
							var product_is_compatible   = product_intersection.length > 0;

							var variation_is_compatible = true;

							if ( product_is_compatible ) {

								if ( product_type === 'variable' ) {

									var variation_id = component.$self.find( '.single_variation_wrap .variations_button input[name="variation_id"]' ).val();

									if ( variation_id > 0 ) {

										var variation_in_scenarios = item_scenario_data[ variation_id ];

										product_intersection    = wc_cp_intersect_safe( product_intersection, variation_in_scenarios );
										variation_is_compatible = product_intersection.length > 0;
									}
								}
							}

							var is_compatible = product_is_compatible && variation_is_compatible;

							if ( is_compatible ) {
								scenario_shaping_components_excl_current.push( component_id );
								active_scenarios_excl_current = product_intersection;
							}
						}

					} );
				}


				if ( wc_composite_params.script_debug === 'yes' ) {
					wc_cp_log( tabs + 'Removing active scenarios where all scenario shaping components are masked...' );
				}

				if ( wc_composite_params.script_debug === 'yes' ) {
					wc_cp_log( tabs + '	Scenario shaping components: ' + scenario_shaping_components_incl_current.toString() );
				}

				if ( return_result ) {

					var result = {
						incl_current: composite.get_binding_scenarios( active_scenarios_incl_current, scenario_shaping_components_incl_current ),
						excl_current: composite.get_binding_scenarios( active_scenarios_excl_current, scenario_shaping_components_excl_current )
					};

					if ( wc_composite_params.script_debug === 'yes' ) {
						wc_cp_log( '\n' + tabs + 'Final active scenarios incl.: ' + result.incl_current.toString() + '\n' + tabs + 'Final active scenarios excl.: ' + result.excl_current.toString() );
					}

					return result;

				} else {

					composite.active_scenarios = {
						incl_current: composite.get_binding_scenarios( active_scenarios_incl_current, scenario_shaping_components_incl_current ),
						excl_current: composite.get_binding_scenarios( active_scenarios_excl_current, scenario_shaping_components_excl_current )
					};

					composite.$composite_data.data( 'active_scenarios', composite.active_scenarios.incl_current );

					if ( wc_composite_params.script_debug === 'yes' ) {
						wc_cp_log( '\n' + tabs + 'Final active scenarios incl.: ' + composite.active_scenarios.incl_current.toString() + '\n' + tabs + 'Final active scenarios excl.: ' + composite.active_scenarios.excl_current.toString() );
					}
				}

			},

			/**
			 * Filters out unbinding scenarios
			 */

			get_binding_scenarios: function( scenarios, scenario_shaping_components ) {

				var composite = this;

				var masked    = composite.get_scenario_data().scenario_settings.masked_components;
				var clean     = [];

				if ( scenario_shaping_components.length > 0 ) {

					if ( scenarios.length > 0 ) {
						$.each( scenarios, function( i, scenario_id ) {

							// If all scenario shaping components are masked, filter out the scenario
							var all_components_masked_in_scenario = true;

							$.each( scenario_shaping_components, function( i, component_id ) {

								if ( $.inArray( component_id.toString(), masked[ scenario_id ] ) == -1 ) {
									all_components_masked_in_scenario = false;
									return false;
								}
							} );

							if ( ! all_components_masked_in_scenario ) {
								clean.push( scenario_id );
							}
						} );
					}

				} else {
					clean = scenarios;
				}

				if ( clean.length === 0 && scenarios.length > 0 ) {
					clean = scenarios;
				}

				return clean;

			},

			/**
			 * Filters active scenarios by type
			 */

			get_active_scenarios_by_type: function( type ) {

				var composite   = this;

				var incl = composite.get_scenarios_by_type( composite.active_scenarios.incl_current, type );
				var excl = composite.get_scenarios_by_type( composite.active_scenarios.excl_current, type );

				return {
					incl_current: incl,
					excl_current: excl
				};

			},

			/**
			 * Filters scenarios by type
			 */

			get_scenarios_by_type: function( scenarios, type ) {

				var composite   = this;

				var filtered    = [];
				var scenario_id = '';

				if ( scenarios.length > 0 ) {
					for ( var i in scenarios ) {

						scenario_id = scenarios[ i ];

						if ( $.inArray( type, composite.get_scenario_data().scenario_settings.scenario_actions[ scenario_id ] ) > -1 ) {
							filtered.push( scenario_id );
						}
					}
				}

				return filtered;

			},

			/**
			 * Filters out masked scenarios in 'update_selections'
			 */

			get_binding_scenarios_for: function( scenarios, component_id ) {

				var composite = this;

				var masked    = composite.get_scenario_data().scenario_settings.masked_components;
				var clean     = [];

				if ( scenarios.length > 0 ) {
					$.each( scenarios, function( i, scenario_id ) {

						if ( $.inArray( component_id.toString(), masked[ scenario_id ] ) == -1 ) {
							clean.push( scenario_id );
						}

					} );
				}

				return clean;

			},

			/**
			 * Activates or deactivates products and variations based on scenarios
			 */

			update_selections: function( firing_step_id, excl_firing ) {

				var composite        = this;

				var style            = composite.composite_layout;
				var selection_mode   = composite.composite_selection_mode;

				var fired_by_step;

				if ( style !== 'single' ) {
					fired_by_step  = composite.get_current_step();
					firing_step_id = fired_by_step.step_id;
				} else {
					fired_by_step = composite.get_step( firing_step_id );
				}

				var active_scenarios = [];
				var tabs             = '';

				for ( var i = composite.actions_nesting-1; i > 0; i--) {
					tabs = tabs + '	';
				}

				if ( typeof( excl_firing ) === 'undefined' ) {
					excl_firing = false;
				}

				if ( style === 'progressive' || style === 'paged' ) {
					excl_firing = true;
				}

				if ( wc_composite_params.script_debug === 'yes' ) {
					wc_cp_log( '\n' + tabs + 'Updating selections...' );
				}

				/**
				 * 	1. Clear resets
				 */

				if ( wc_composite_params.script_debug === 'yes' ) {
					wc_cp_log( '\n' + tabs + 'Clearing incompatible selections...' );
				}

				$.each( composite.composite_components, function( index, component ) {

					var component_id             = component.component_id;
					var component_options_select = component.$component_options.find( 'select.component_options_select' );

					var product_id               = component.get_selected_product_id();

					if ( style !== 'single' && firing_step_id != component_id ) {
						return true;
					}

					// If a disabled option is still selected, val will be null - use this fact to reset options before moving on
					if ( product_id === null ) {
						component.$self.addClass( 'reset' );

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + component.get_title() + ' selection was found disabled.' );
						}
					}

					// Verify and reset active product selections that were found incompatible
					if ( component.$self.hasClass( 'reset' ) ) {

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + 'Resetting ' + component.get_title() + '...\n' );
						}

						component.$self.addClass( 'resetting' );

						composite.has_scenarios_update_lock = true;
						component_options_select.val( '' ).change();
						composite.has_scenarios_update_lock = false;

						component.$self.removeClass( 'reset' );
						component.$self.removeClass( 'resetting' );

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + 'Reset ' + component.get_title() + ' complete...\n' );
						}

					}

					// Verify and reset active variation selections that were found incompatible
					if ( component.$self.hasClass( 'reset_variation' ) ) {

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + 'Resetting variation selections of ' + component.get_title() + '...\n' );
						}

						component.$self.addClass( 'resetting' );

						composite.has_scenarios_update_lock = true;
						component.$component_summary.find( '.reset_variations' ).trigger( 'click' );
						composite.has_scenarios_update_lock = false;

						component.$self.removeClass( 'reset_variation' );
						component.$self.removeClass( 'resetting' );

						setTimeout( function() {
							component.$self.find( '.reset_variations' ).css( 'visibility', 'hidden' );
						}, 10 );

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + 'Reset variation selections of ' + component.get_title() + ' complete...\n' );
						}

					}

				} );

				/**
				 * 	2. Disable or enable product and variation selections
				 */

				var firing_step_index = fired_by_step.step_index;

				if ( fired_by_step.is_review() ) {
					firing_step_index = 1000;
				}

				// Get active scenarios filtered by action = 'compat_group'
				var scenarios = composite.get_active_scenarios_by_type( 'compat_group' );

				// Do the work
				$.each( composite.composite_components, function( index, component ) {

					var component_id    = component.component_id;
					var summary_content = component.$self.find( '.component_summary > .content' );

					if ( wc_composite_params.script_debug === 'yes' ) {
						wc_cp_log( tabs + 'Updating selections of ' + component.get_title() + '...' );
					}

					if ( style === 'single' && selection_mode === 'thumbnails' ) {

						// The constraints of the working item must not be taken into account when refreshing thumbnails using the 'single' layout, in order to be able to switch the selection
						var component_scenarios = composite.update_active_scenarios( component_id, true );
						active_scenarios        = composite.get_scenarios_by_type( component_scenarios.excl_current, 'compat_group' );

					} else {

						// The constraints of the firing item must not be taken into account when the update action is triggered by a dropdown, in order to be able to switch the selection
						if ( excl_firing && firing_step_index == index ) {
							active_scenarios = scenarios.excl_current;
						} else {
							active_scenarios = scenarios.incl_current;
						}
					}

					if ( wc_composite_params.script_debug === 'yes' ) {
						wc_cp_log( tabs + '	Reference scenarios: ' + active_scenarios.toString() );
						wc_cp_log( tabs + '	Removing any scenarios where the current component is masked...' );
					}

					active_scenarios = composite.get_binding_scenarios_for( active_scenarios, component_id );

					// Enable all if all active scenarios ignore this component
					if ( active_scenarios.length === 0 ) {
						active_scenarios.push( '0' );
					}

					if ( wc_composite_params.script_debug === 'yes' ) {
						wc_cp_log( tabs + '	Active scenarios: ' + active_scenarios.slice().toString() );
					}

					var scenario_data      = composite.get_scenario_data().scenario_data;
					var item_scenario_data = scenario_data[ component_id ];

					// Set optional status

					var is_optional = false;

					if ( 0 in item_scenario_data ) {

						var optional_in_scenarios = item_scenario_data[ 0 ];

						for ( var s in optional_in_scenarios ) {

							var optional_in_scenario_id = optional_in_scenarios[ s ];

							if ( $.inArray( optional_in_scenario_id, active_scenarios ) > -1 ) {
								is_optional = true;
								break;
							}
						}

					} else {
						is_optional = false;
					}

					if ( is_optional ) {
						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + '	Component set as optional.' );
						}
						component.set_optional( true );
					} else {
						component.set_optional( false );
					}

					/*
					 * Disable incompatible products
					 */

					var thumbnails;
					var thumbnail_loop    = 0;
					var thumbnail_columns = 1;

					if ( composite.composite_selection_mode === 'thumbnails' ) {
						thumbnails        = component.$component_options.find( '.component_option_thumbnails' );
						thumbnail_columns = parseInt( thumbnails.data( 'columns' ) );
					}

					var component_options_select       = component.$component_options.find( 'select.component_options_select' );
					var component_options_select_value = component.get_selection_id();

					if ( ! component_options_select_value ) {
						component_options_select_value = component_options_select.val();
					}

					// Reset options
					if ( ! component_options_select.data( 'select_options' ) ) {
						component_options_select.data( 'select_options', component_options_select.find( 'option:gt(0)' ).get() );
					}

					component_options_select.find( 'option:gt(0)' ).remove();
					component_options_select.append( component_options_select.data( 'select_options' ) );
					component_options_select.find( 'option:gt(0)' ).removeClass( 'disabled' );
					component_options_select.find( 'option:gt(0)' ).removeAttr( 'disabled' );
					component.set_selection_invalid( false );

					if ( composite.composite_selection_mode === 'thumbnails' ) {
						thumbnails.find( '.no_compat_results' ).remove();
					}

					// Enable or disable options
					component_options_select.find( 'option:gt(0)' ).each( function() {

						var product_id           = $(this).val();
						var product_in_scenarios = item_scenario_data[ product_id ];
						var is_compatible        = false;

						var thumbnail;
						var thumbnail_container;

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + '	Updating selection #' + product_id + ':' );
							wc_cp_log( tabs + '		Selection in scenarios: ' + product_in_scenarios.toString() );
						}

						for ( var i in product_in_scenarios ) {

							var scenario_id = product_in_scenarios[ i ];

							if ( $.inArray( scenario_id, active_scenarios ) > -1 ) {
								is_compatible = true;
								break;
							}
						}

						if ( ! is_compatible ) {

							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '		Selection disabled.' );
							}

							if ( component_options_select_value != product_id ) {
								$(this).addClass( 'disabled' );
							} else {
								component.set_selection_invalid( true );
								$(this).prop( 'disabled', 'disabled' );
							}

							if ( composite.composite_selection_mode === 'thumbnails' ) {

								thumbnail = component.$component_options.find( '#component_option_thumbnail_' + $(this).val() );

								thumbnail.addClass( 'disabled' );

								if ( component.hide_disabled_products() && ( style === 'single' || index >= fired_by_step.step_index ) ) {

									thumbnail_container = thumbnail.closest( '.component_option_thumbnail_container' );

									// Hide incompatible li elements
									thumbnail_container.removeClass( 'first last' ).addClass( 'hidden' );
								}
							}

						} else {

							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '		Selection enabled.' );
							}

							if ( composite.composite_selection_mode === 'thumbnails' ) {

								thumbnail = component.$component_options.find( '#component_option_thumbnail_' + $(this).val() );

								thumbnail.removeClass( 'disabled' );

								if ( component.hide_disabled_products() && ( style === 'single' || index >= fired_by_step.step_index ) ) {

									thumbnail_loop++;

									thumbnail_container = thumbnail.closest( '.component_option_thumbnail_container' );
									var thumbnail_class = '';

									// Add first/last class to compatible li elements
									if ( ( ( thumbnail_loop - 1 ) % thumbnail_columns ) == 0 || thumbnail_columns == 1 ) {
										thumbnail_class = 'first';
									}

									if ( thumbnail_loop % thumbnail_columns == 0 ) {
										thumbnail_class += ' last';
									}

									thumbnail_container.removeClass( 'first last hidden' );

									if ( thumbnail_class ) {
										thumbnail_container.addClass( thumbnail_class );
									}
								}
							}
						}

					} );

					// Hide or grey-out disabled options
					if ( component.hide_disabled_products() ) {
						component_options_select.find( 'option.disabled' ).remove();

						if ( composite.composite_selection_mode === 'thumbnails' ) {

							var thumbnail_elements         = thumbnails.find( '.component_option_thumbnail_container' );
							var visible_thumbnail_elements = thumbnail_elements.not( '.hidden' );

							if ( thumbnail_elements.length > 0 && visible_thumbnail_elements.length == 0 ) {
								thumbnails.find( '.component_option_thumbnails_container' ).after( '<p class="no_compat_results">' + wc_composite_params.i18n_no_compat_options + '</p>' );
								component.has_compat_results   = false;
								component.compat_results_count = 0;
							} else {
								component.has_compat_results   = true;
								component.compat_results_count = visible_thumbnail_elements.length;
							}
						}

					} else {
						component_options_select.find( 'option.disabled' ).prop( 'disabled', 'disabled' );
					}

					/*
					 * Disable incompatible variations
					 */

					var product_type = component.get_selected_product_type();

					if ( product_type === 'variable' ) {

						// Note the variation id
						var variation_input    = summary_content.find( '.single_variation_wrap .variations_button input[name="variation_id"]' );
						var variation_input_id = variation_input.val();
						var variation_valid    = variation_input_id > 0 ? false : true;

						if ( wc_composite_params.script_debug === 'yes' ) {
							wc_cp_log( tabs + '		Checking variations...' );
						}

						if ( variation_input_id > 0 ) {
							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '			--- Stored variation is #' + variation_input_id );
							}
						}

						// Get all variations
						var product_variations = component.$component_data.data( 'product_variations' );

						var product_variations_in_scenario = [];

						for ( var i in product_variations ) {

							var variation_id           = product_variations[ i ].variation_id;
							var variation_in_scenarios = item_scenario_data[ variation_id ];
							var is_compatible          = false;

							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '			Checking variation #' + variation_id + ':' );
								wc_cp_log( tabs + '			Selection in scenarios: ' + variation_in_scenarios.toString() );
							}

							for ( var k in variation_in_scenarios ) {

								var scenario_id = variation_in_scenarios[ k ];

								if ( $.inArray( scenario_id, active_scenarios ) > -1 ) {
									is_compatible = true;
									break;
								}
							}

							// Copy all variation objects but set the variation_is_active property to false in order to disable the attributes of incompatible variations
							// Only if WC v2.3 and disabled variations are set to be visible
							if ( wc_composite_params.is_wc_version_gte_2_3 === 'yes' && ! component.hide_disabled_variations() ) {

								var variation = $.extend( true, {}, product_variations[ i ] );

								var variation_has_empty_attributes = false;

								if ( ! is_compatible ) {

									variation.variation_is_active = false;

									// do not include incompatible variations with empty attributes - they can break stuff when prioritized
									for ( var attr_name in variation.attributes ) {
										if ( variation.attributes[ attr_name ] === '' ) {
											variation_has_empty_attributes = true;
											break;
										}
									}

									if ( wc_composite_params.script_debug === 'yes' ) {
										wc_cp_log( tabs + '			Variation disabled.' );
									}
								} else {

									if ( wc_composite_params.script_debug === 'yes' ) {
										wc_cp_log( tabs + '			Variation enabled.' );
									}

									if ( parseInt( variation_id ) === parseInt( variation_input_id ) ) {
										variation_valid = true;
										if ( wc_composite_params.script_debug === 'yes' ) {
											wc_cp_log( tabs + '			--- Stored variation is valid.' );
										}
									}
								}

								if ( ! variation_has_empty_attributes ) {

									product_variations_in_scenario.push( variation );
								}

							// Copy only compatible variations
							// Only if WC v2.1/2.2 or disabled variations are set to be hidden
							} else {

								if ( is_compatible ) {

									product_variations_in_scenario.push( product_variations[ i ] );

									if ( wc_composite_params.script_debug === 'yes' ) {
										wc_cp_log( tabs + '			Variation enabled.' );
									}

									if ( parseInt( variation_id ) === parseInt( variation_input_id ) ) {
										variation_valid = true;
										if ( wc_composite_params.script_debug === 'yes' ) {
											wc_cp_log( tabs + '			--- Stored variation is valid.' );
										}
									}

								} else {
									if ( wc_composite_params.script_debug === 'yes' ) {
										wc_cp_log( tabs + '			Variation disabled.' );
									}
								}
							}
						}

						// Put filtered variations in place
						summary_content.data( 'product_variations', product_variations_in_scenario );

						if ( ! variation_valid ) {

							if ( wc_composite_params.script_debug === 'yes' ) {
								wc_cp_log( tabs + '			--- Stored variation was NOT found.' );
							}

							composite.has_scenarios_update_lock = true;
							summary_content.find( '.reset_variations' ).trigger( 'click' );
							composite.has_scenarios_update_lock = false;
						}

						summary_content.triggerHandler( 'reload_product_variations' );
					}

					component.$self.trigger( 'wc-composite-component-selections-updated' );

				} );

				if ( wc_composite_params.script_debug === 'yes' ) {
					wc_cp_log( tabs + 'Finished updating component selections.\n\n' );
				}

			},

			/**
			 * Uses a dumb scheduler to update all pagination/navigation ui elements
			 */

			update_ui: function( delay_nav_update ) {

				var composite    = this;
				var current_step = composite.get_current_step();

				if ( typeof( delay_nav_update ) === 'undefined' ) {
					delay_nav_update = false;
				}

				// Update nav previous/next button immediately before init
				if ( composite.composite_initialized === false ) {
					delay_nav_update = false;
				}

				if ( delay_nav_update ) {
					composite.has_update_nav_delay = true;
				}

				// Dumb task scheduler
				if ( composite.has_ui_update_lock === true ) {
					return false;
				}

				composite.has_ui_update_lock = true;

				setTimeout( function() {

					composite.update_ui_task( composite.has_update_nav_delay );
					composite.has_ui_update_lock   = false;
					composite.has_update_nav_delay = false;

					current_step.get_element().trigger( 'wc-composite-ui-updated' );

				}, 10 );

			},

			/**
			 * Updates all pagination/navigation ui elements
			 */

			update_ui_task: function( delay_nav_update ) {

				var composite           = this;

				var current_step        = composite.get_current_step();
				var item                = current_step.get_element();

				/**
				 * Update navigation (next/previous buttons)
				 */

				var form                = composite.$composite_form;
				var navigation          = composite.$composite_navigation;
				var style               = composite.composite_layout;
				var style_variation     = composite.composite_layout_variation;
				var show_next           = false;

				var next_step           = composite.get_next_step();
				var prev_step           = composite.get_previous_step();

				var button_next         = navigation.find( '.next' );
				var button_prev         = navigation.find( '.prev' );

				var update_nav_duration = delay_nav_update && style !== 'progressive' ? 290 : 10;

				setTimeout( function() {

					if ( style === 'single' ) {
						return false;
					}

					// hide navigation
					button_next.addClass( 'invisible inactive' );
					button_prev.addClass( 'invisible' );

					composite.$component_message.addClass( 'inactive' );

					if ( current_step.is_component() ) {

						// selectively show next/previous navigation buttons
						if ( next_step && style_variation !== 'componentized' ) {

							button_next.html( wc_composite_params.i18n_next_step.replace( '%s', next_step.get_title() ) );
							button_next.removeClass( 'invisible' );

							if ( next_step.get_element().hasClass( 'toggled' ) ) {
								next_step.get_element().find( '.component_title' ).removeClass( 'inactive' );
							}

						} else if ( style === 'paged' ) {
							button_next.html( wc_composite_params.i18n_final_step );
							button_next.removeClass( 'invisible' );
						}
					}

					// paged previous / next
					if ( current_step.validate_inputs() || ( style_variation === 'componentized' && current_step.is_component() ) ) {

						if ( next_step ) {
							button_next.removeClass( 'inactive' );
						}

						if ( prev_step && style === 'paged' && prev_step.is_component() ) {
							button_prev.html( wc_composite_params.i18n_previous_step.replace( '%s', prev_step.get_title() ) );
							button_prev.removeClass( 'invisible' );
						} else {
							button_prev.html( '' );
						}

						show_next = true;

					} else {

						if ( next_step && next_step.get_element().hasClass( 'toggled' ) ) {
							next_step.get_element().find( '.component_title' ).addClass( 'inactive' );
						}

						if ( prev_step && prev_step.is_component() ) {

							var product_id = prev_step.get_component().get_selected_product_id();

							if ( product_id > 0 || product_id === '0' || product_id === '' && prev_step.get_component().is_optional() ) {

								if ( style === 'paged' ) {
									button_prev.html( wc_composite_params.i18n_previous_step.replace( '%s', prev_step.get_title() ) );
									button_prev.removeClass( 'invisible' );
								}
							}
						}

						if ( current_step.is_component() ) {

							// don't show the prompt if it's the last component of the progressive layout
							if ( ! item.hasClass( 'last' ) || ! item.hasClass( 'progressive' ) ) {

								if ( current_step.get_component().get_selected_product_id() > 0 ) {
									if ( ! current_step.get_component().is_in_stock() ) {
										composite.$component_message_content.html( wc_composite_params.i18n_selected_component_options_no_stock.replace( '%s', current_step.get_title() ) );
									} else {
										composite.$component_message_content.html( wc_composite_params.i18n_select_component_options.replace( '%s', current_step.get_title() ) );
									}
								} else {
									composite.$component_message_content.html( wc_composite_params.i18n_select_component_option.replace( '%s', current_step.get_title() ) );
								}

								composite.$component_message.removeClass( 'inactive' );
							}
						}

					}

					// move navigation and component message container into the next component when using the progressive layout without toggles
					if ( style === 'progressive' ) {

						var navi      = form.find( '.composite_navigation.progressive' );
						var item_navi = item.find( '.composite_navigation.progressive' );
						var next_item = form.find( '.component.next' );

						if ( item_navi.length == 0 ) {

							navi.css( { visibility: 'hidden' } );

							composite.$component_message.slideUp( 200 );
							navi.slideUp( 200 );

							setTimeout( function() {

								composite.$component_message.appendTo( item.find( '.component_inner' ) );
								navi.appendTo( item.find( '.component_inner' ) ).css( { visibility: 'visible' } );

								setTimeout( function() {

									if ( ! composite.$component_message.hasClass( 'inactive' ) ) {
										composite.$component_message.slideDown( 200 );
									}

									var show_navi = false;

									if ( ! item.hasClass( 'last' ) ) {
										if ( show_next && ! next_item.hasClass( 'toggled' ) ) {
											show_navi = true;
										}
									}

									if ( show_navi ) {
										navi.slideDown( { duration: 200, queue: false } );
									}

								}, 200 );

							}, 200 );

						} else {

							if ( composite.$component_message.hasClass( 'inactive' ) ) {
								composite.$component_message.slideUp( 200 );
							} else {
								composite.$component_message.slideDown( 200 );
							}

							var show_navi = false;

							if ( ! item.hasClass( 'last' ) ) {
								if ( show_next && ! next_item.hasClass( 'toggled' ) ) {
									show_navi = true;
								}
							}

							if ( show_navi ) {
								navi.slideDown( 200 );
							} else {
								navi.slideUp( 200 );
							}
						}

					// move component message container into the next component when using a paged layout with thumbnails
					} else if ( style === 'paged' && composite.composite_selection_mode === 'thumbnails' ) {

						var item_message_container = item.find( '.component_message' );

						if ( item_message_container.length == 0 && current_step.is_component() ) {
							current_step.get_component().$component_selections.prepend( composite.$component_message.hide() );
						}

						if ( composite.$component_message.hasClass( 'inactive' ) ) {
							composite.$component_message.slideUp( 200 );
						} else {
							composite.$component_message.slideDown( 200 );
						}

					} else {

						if ( composite.$component_message.hasClass( 'inactive' ) ) {
							composite.$component_message.slideUp( 200 );
						} else {
							composite.$component_message.slideDown( 200 );
						}
					}

				}, update_nav_duration );

				/**
				 * Update pagination (step pagination + summary sections)
				 */

				var pagination = composite.$composite_pagination;
				var summary    = composite.$composite_summary;

				if ( pagination.length == 0 && summary.length == 0 ) {
					return false;
				}

				var deactivate_step_links = false;

				$.each( composite.composite_steps, function( step_index, step ) {

					if ( step_index > 0 ) {

						var prev_step = composite.composite_steps[ step_index - 1 ];

						if ( ! prev_step.is_review() && style !== 'single' ) {

							if ( false === prev_step.validate_inputs() && ( style_variation !== 'componentized' || ( style_variation === 'componentized' && composite.composite_sequential_comp_progress === 'yes' ) ) ) {
								deactivate_step_links = true;
							} else if ( prev_step.is_blocked() ) {
								deactivate_step_links = true;
							// Don't activate new step links when going back
							} else if ( step.view_elements.$pagination_element_link.hasClass( 'inactive' ) && composite.get_next_step().step_index != step_index && current_step.step_index < composite.last_active_step_index && step_index > composite.last_active_step_index ) {
								deactivate_step_links = true;
							}
						}
					}

					// Update simple pagination
					if ( pagination.length > 0 ) {

						if ( step.is_current() ) {

							step.view_elements.$pagination_element_link.addClass( 'inactive' );
							step.view_elements.$pagination_element.addClass( 'pagination_element_current' );

						} else {

							if ( deactivate_step_links ) {

								step.view_elements.$pagination_element_link.addClass( 'inactive' );
								step.view_elements.$pagination_element.removeClass( 'pagination_element_current' );

							} else {

								step.view_elements.$pagination_element_link.removeClass( 'inactive' );
								step.view_elements.$pagination_element.removeClass( 'pagination_element_current' );

							}
						}
					}

					// Update summary links
					if ( summary.length > 0 ) {

						if ( step.is_current() ) {

							step.view_elements.$summary_element_link.removeClass( 'disabled' );

							if ( style !== 'single' ) {
								step.view_elements.$summary_element_link.addClass( 'selected' );
							}

							if ( composite.get_step( 'review' ) && composite.get_step( 'review' ).get_element().is( ':visible' ) && ! composite.get_step( 'review' ).get_element().hasClass( 'faded' ) ) {
								step.view_elements.$summary_element.find( '.summary_element_selection_prompt' ).slideUp( 200 );
							}

						} else {

							step.view_elements.$summary_element.find( '.summary_element_selection_prompt' ).slideDown( 200 );

							if ( deactivate_step_links ) {

								step.view_elements.$summary_element_link.removeClass( 'selected' );
								step.view_elements.$summary_element_link.addClass( 'disabled' );

							} else {

								step.view_elements.$summary_element_link.removeClass( 'disabled' );
								step.view_elements.$summary_element_link.removeClass( 'selected' );

							}
						}

					}

				} );

				// Update widget
				composite.composite_summary_widget.update_links();

			},

			/**
			 * Updates the state of the Review/Summary template
			 */

			update_summary: function() {

				var composite         = this;

				var composite_summary = composite.$composite_summary;
				var price_data        = composite.$composite_data.data( 'price_data' );

				if ( composite_summary.length == 0 ) {
					return false;
				}

				$.each( composite.composite_components, function( index, component ) {

					var component_id       = component.component_id;
					var item               = component.$self;
					var item_id            = component_id;

					var item_summary_outer = component.get_step().view_elements.$summary_element_wrapper;
					var item_summary_inner = component.get_step().view_elements.$summary_element_inner;

					var selections         = component.$component_options.find( '#component_options_' + component_id );
					var product_type       = component.get_selected_product_type();
					var product_id         = component.get_selected_product_id();
					var qty                = parseInt( item.find( '.component_wrap input.qty' ).val() );

					var title              = '';
					var select             = '';
					var image              = '';

					var product_title      = '';
					var product_quantity   = '';
					var product_meta       = '';
					var load_height        = 0;

					// lock height if animating
					if ( composite_summary.is( ':visible' ) ) {
						load_height = item_summary_inner.outerHeight( true );
						item_summary_outer.css( 'height', load_height );
					}

					// Get title and image
					if ( product_type === 'none' ) {

						if ( component.is_optional() ) {
							title = selections.find( 'option.none' ).data( 'title' );
						}

					} else if ( product_type === 'variable' ) {

						if ( product_id > 0 && ( qty > 0 || qty === 0 ) ) {

							product_title    = selections.find( 'option:selected' ).data( 'title' );
							product_quantity = '<strong>' + wc_composite_params.i18n_qty_string.replace( '%s', qty ) + '</strong>';
							product_title    = wc_composite_params.i18n_title_string.replace( '%t', product_title ).replace( '%q', product_quantity ).replace( '%p', '' );
							product_meta     = wc_cp_get_variable_product_attributes_description( item.find( '.variations' ) );

							if ( product_meta ) {
								title = wc_composite_params.i18n_selected_product_string.replace( '%t', product_title ).replace( '%m', product_meta );
							} else {
								title = product_title;
							}

							image = item.find( '.composited_product_image img' ).attr( 'src' );

							if ( typeof( image ) === 'undefined' ) {
								image = selections.find( 'option:selected' ).data( 'image_src' );
							}
						}

					} else if ( product_type === 'bundle' ) {

						if ( product_id > 0 && ( qty > 0 || qty === 0 ) ) {

							var selected_bundled_products = '';
							var bundled_products_num      = 0;

							item.find( '.bundled_product .cart' ).each( function() {

								if ( $(this).data( 'quantity' ) > 0 )
									bundled_products_num++;
							} );

							if ( bundled_products_num == 0 ) {

								title = wc_composite_params.i18n_none;

							} else {

								item.find( '.bundled_product .cart' ).each( function() {

									if ( $(this).data( 'quantity' ) > 0 ) {

										var item_title    = $(this).data( 'title' );
										var item_quantity = '<strong>' + wc_composite_params.i18n_qty_string.replace( '%s', parseInt( $(this).data( 'quantity' ) * qty ) ) + '</strong>';
										var item_meta     = wc_cp_get_variable_product_attributes_description( $(this).find( '.variations' ) );

										item_title = wc_composite_params.i18n_title_string.replace( '%t', item_title ).replace( '%q', item_quantity ).replace( '%p', '' );

										if ( item_meta ) {
											item_title = wc_composite_params.i18n_selected_product_string.replace( '%t', item_title ).replace( '%m', item_meta );
										}

										selected_bundled_products = selected_bundled_products + item_title + '</br>';
									}
								} );

								title = selected_bundled_products;
							}

							image = selections.find( 'option:selected' ).data( 'image_src' );
						}

					} else {

						if ( product_id > 0 ) {

							product_title    = selections.find( 'option:selected' ).data( 'title' );
							product_quantity = isNaN( qty ) ? '' : '<strong>' + wc_composite_params.i18n_qty_string.replace( '%s', qty ) + '</strong>';

							title = wc_composite_params.i18n_title_string.replace( '%t', product_title ).replace( '%q', product_quantity ).replace( '%p', '' );
							image = selections.find( 'option:selected' ).data( 'image_src' );
						}
					}

					// Selection text
					if ( title && component.is_configured() ) {
						if ( item.hasClass( 'static') ) {
							select = '<a href="">' + wc_composite_params.i18n_summary_static_component + '</a>';
						} else {
							select = '<a href="">' + wc_composite_params.i18n_summary_configured_component + '</a>';
						}
					} else {
						select = '<a href="">' + wc_composite_params.i18n_summary_empty_component + '</a>';
					}

					// Update title
					if ( title ) {
						component.get_step().view_elements.$summary_element_title.html( '<span class="summary_element_content">' + title + '</span><span class="summary_element_content summary_element_selection_prompt">' + select + '</span>' );
					} else {
						component.get_step().view_elements.$summary_element_title.html( '<span class="summary_element_content summary_element_selection_prompt">' + select + '</span>' );
					}

					// Update element class
					if ( component.is_configured() ) {
						item_summary_outer.addClass( 'configured' );
					} else {
						item_summary_outer.removeClass( 'configured' );
					}

					// Hide selection text
					if ( $(this).hasClass( 'active' ) ) {
						component.get_step().view_elements.$summary_element_title.find( '.summary_element_selection_prompt' ).hide();
					}

					// Update image
					composite.update_summary_element_image( component, image );

					// Update price
					if ( price_data[ 'per_product_pricing' ] === true && product_id > 0 && qty > 0 && component.get_step().validate_inputs() ) {

						var price         = ( parseFloat( price_data[ 'prices' ][ item_id ] ) + parseFloat( price_data[ 'addons_prices' ][ item_id ] ) ) * qty;
						var regular_price = ( parseFloat( price_data[ 'regular_prices' ][ item_id ] ) + parseFloat( price_data[ 'addons_prices' ][ item_id ] ) ) * qty;

						var price_format         = wc_cp_woocommerce_number_format( wc_cp_number_format( price ) );
						var regular_price_format = wc_cp_woocommerce_number_format( wc_cp_number_format( regular_price ) );

						if ( regular_price > price ) {
							component.get_step().view_elements.$summary_element_price.html( '<span class="price summary_element_content"><del>' + regular_price_format + '</del> <ins>' + price_format + '</ins></span>' );
						} else {
							component.get_step().view_elements.$summary_element_price.html( '<span class="price summary_element_content">' + price_format + '</span>' );
						}

					} else {
						component.get_step().view_elements.$summary_element_price.html( '' );
					}

					// Send an event to allow 3rd party code to add data to the summary
					item.trigger( 'wc-composite-component-update-summary-content' );

					// Animate
					if ( composite_summary.is( ':visible' ) ) {

						// measure height
						var new_height     = item_summary_inner.outerHeight( true );
						var animate_height = false;

						if ( Math.abs( new_height - load_height ) > 1 ) {
							animate_height = true;
						} else {
							item_summary_outer.css( 'height', 'auto' );
						}

						if ( animate_height ) {
							item_summary_outer.animate( { 'height': new_height }, { duration: 200, queue: false, always: function() {
								item_summary_outer.css( { 'height': 'auto' } );
							} } );
						}
					}

				} );

				// Update Summary Widget
				composite.composite_summary_widget.update_markup();

			},

			/**
			 * Updates images in the Review/Summary template
	 		 */

			update_summary_element_image: function( component, img_src ) {

				var element_image = component.get_step().view_elements.$summary_element_image.find( 'img' );

				if ( element_image.length == 0 || element_image.hasClass( 'norefresh' ) ) {
					return false;
				}

				var o_src = element_image.attr( 'data-o_src' );

				if ( ! img_src ) {

					if ( typeof( o_src ) !== 'undefined' ) {
						element_image.attr( 'src', o_src );
					}

				} else {

					if ( typeof( o_src ) === 'undefined' ) {
						o_src = ( ! element_image.attr( 'src' ) ) ? '' : element_image.attr( 'src' );
						element_image.attr( 'data-o_src', o_src );
					}

					element_image.attr( 'src', img_src );
				}

			},

			/**
			 * Schedules an update of the composite totals and review/summary section
			 * Uses a dumb scheduler to avoid queueing multiple calls of update_composite_task() - the "scheduler" simply introduces a 50msec execution delay during which all update requests are dropped
			 */

			update_composite: function() {

				var composite = this;

				// Break out if the initialization is not finished yet (function call triggered by a 'wc-composite-component-loaded' event listener)
				if ( composite.composite_initialized !== true ) {
					return false;
				}

				// Dumb task scheduler
				if ( composite.has_update_lock === true ) {
					return false;
				}

				composite.has_update_lock = true;

				setTimeout( function() {

					composite.update_composite_task();
					composite.has_update_lock = false;

				}, 50 );

			},

			/**
			 * Updates the composite totals and review/summary section + enables/disables the add-to-cart button
			 */

			update_composite_task: function() {

				var composite      = this;
				var composite_data = composite.$composite_data;

				var all_set            = true;
				var component_quantity = {};
				var out_of_stock       = [];

				var price_data         = composite_data.data( 'price_data' );

				/**
				 * Validate components
				 */

				$.each( composite.composite_components, function( index, component ) {

					var component_id    = component.component_id;
					var item            = component.$self;
					var item_id         = component_id;
					var form_data       = composite.$composite_button.find( '.form_data_' + item_id );
					var product_type    = component.get_selected_product_type();

					// Verify submit form input data
					var product_input   = item.find( '#component_options_' + item_id ).val();
					var quantity_input  = item.find( '.component_wrap input.qty' ).val();
					var variation_input = form_data.find( 'input.variation_input' ).val();

					// Reset "configured" status
					component.set_configured( false );

					// Copy prices
					price_data[ 'prices' ][ item_id ]         = parseFloat( component.$component_data.data( 'price' ) );
					price_data[ 'regular_prices' ][ item_id ] = parseFloat( component.$component_data.data( 'regular_price' ) );

					// Save addons prices
					price_data[ 'addons_prices' ][ item_id ] = 0;

					item.find( '.addon' ).each( function() {

						if ( product_type === 'bundle' && ! $(this).closest( '.cart' ).hasClass( 'bundle_data' ) ) {
							return true;
						}

						var addon_cost = 0;

						if ( $(this).is('.addon-custom-price') ) {
							addon_cost = $(this).val();
						} else if ( $(this).is('.addon-input_multiplier') ) {
							if( isNaN( $(this).val() ) || $(this).val() == '' ) { // Number inputs return blank when invalid
								$(this).val( '' );
								$(this).closest('p').find('.addon-alert').show();
							} else {
								if( $(this).val() != '' ) {
									$(this).val( Math.ceil( $(this).val() ) );
								}
								$(this).closest('p').find('.addon-alert').hide();
							}
							addon_cost = $(this).data('price') * $(this).val();
						} else if ( $(this).is('.addon-checkbox, .addon-radio') ) {
							if ( $(this).is(':checked') )
								addon_cost = $(this).data('price');
						} else if ( $(this).is('.addon-select') ) {
							if ( $(this).val() )
								addon_cost = $(this).find('option:selected').data('price');
						} else {
							if ( $(this).val() )
								addon_cost = $(this).data('price');
						}

						if ( ! addon_cost )
							addon_cost = 0;

						price_data[ 'addons_prices' ][ item_id ] = parseFloat( price_data[ 'addons_prices' ][ item_id ] ) + parseFloat( addon_cost );

					} );

					if ( typeof( product_type ) === 'undefined' || product_type == '' ) {
						all_set = false;
					} else if ( ! ( product_input > 0 ) && ! component.is_optional() ) {
						all_set = false;
					} else if ( product_type !== 'none' && quantity_input === '' ) {
						all_set = false;
					} else if ( product_type === 'variable' && ( typeof( variation_input ) === 'undefined' || component.$component_data.data( 'component_set' ) == false ) ) {
						all_set = false;
					} else if ( product_type !== 'variable' && product_type !== 'simple' && product_type !== 'none' && component.$component_data.data( 'component_set' ) == false ) {
						all_set = false;
					} else {

						// Set component as configured
						component.set_configured( true );

						// Update quantity data for price calculations
						if ( quantity_input > 0 ) {
							component_quantity[ item_id ] = parseInt( quantity_input );
						} else {
							component_quantity[ item_id ] = 0;
						}
					}

				} );

				/**
				 * Update paged layout summary state
				 */

				composite.update_summary();

				/**
				 * Add to cart button state and price
				 */

				if ( all_set ) {

					if ( ( price_data[ 'per_product_pricing' ] == false ) && ( price_data[ 'price_undefined' ] == true ) ) {
						composite.disable_add_to_cart( wc_composite_params.i18n_unavailable_text );
						return false;
					}

					if ( price_data[ 'per_product_pricing' ] == true ) {

						price_data[ 'total' ]         = 0;
						price_data[ 'regular_total' ] = 0;

						for ( var item_id_ppp in price_data[ 'prices' ] ) {

							price_data[ 'total' ]         += ( parseFloat( price_data[ 'prices' ][ item_id_ppp ] ) + parseFloat( price_data[ 'addons_prices' ][ item_id_ppp ] ) ) * component_quantity[ item_id_ppp ];
							price_data[ 'regular_total' ] += ( parseFloat( price_data[ 'regular_prices' ][ item_id_ppp ] ) + parseFloat( price_data[ 'addons_prices' ][ item_id_ppp ] ) ) * component_quantity[ item_id_ppp ];
						}

						price_data[ 'total' ]         += parseFloat( price_data[ 'base_price' ] );
						price_data[ 'regular_total' ] += parseFloat( price_data[ 'base_regular_price' ] );

					} else {

						price_data[ 'total' ]         = parseFloat( price_data[ 'base_price' ] );
						price_data[ 'regular_total' ] = parseFloat( price_data[ 'base_regular_price' ] );

						for ( var item_id_sp in price_data[ 'addons_prices' ] ) {

							price_data[ 'total' ]         += parseFloat( price_data[ 'addons_prices' ][ item_id_sp ] ) * component_quantity[ item_id_sp ];
							price_data[ 'regular_total' ] += parseFloat( price_data[ 'addons_prices' ][ item_id_sp ] ) * component_quantity[ item_id_sp ];
						}
					}

					var composite_addon = composite_data.find( '#product-addons-total' );

					if ( composite_addon.length > 0 ) {
						composite_addon.data( 'price', price_data[ 'total' ] );
						composite_data.trigger( 'woocommerce-product-addons-update' );
					}

					if ( price_data[ 'total' ] == 0 && price_data[ 'show_free_string' ] == true ) {
						composite.$composite_price.html( '<p class="price"><span class="total">' + wc_composite_params.i18n_total + '</span>'+ wc_composite_params.i18n_free +'</p>' );
					} else {

						var sales_price_format   = wc_cp_woocommerce_number_format( wc_cp_number_format( price_data[ 'total' ] ) );
						var regular_price_format = wc_cp_woocommerce_number_format( wc_cp_number_format( price_data[ 'regular_total' ] ) );

						if ( price_data[ 'regular_total' ] > price_data[ 'total' ] ) {
							composite.$composite_price.html( '<p class="price"><span class="total">' + wc_composite_params.i18n_total + '</span><del>' + regular_price_format + '</del> <ins>' + sales_price_format + '</ins></p>' );
						} else {
							composite.$composite_price.html( '<p class="price"><span class="total">' + wc_composite_params.i18n_total + '</span>' + sales_price_format + '</p>' );
						}
					}

					// Check if any item is out of stock

					var out_of_stock_found = false;

					$.each( composite.composite_components, function( index, component ) {

						if ( false === component.is_in_stock() ) {
							out_of_stock_found = true;
						}

					} );


					if ( composite.composite_button_behaviour !== 'new' ) {
						composite_data.find( '.composite_wrap' ).slideDown( 200 );
					} else {
						composite.$composite_price.removeClass( 'inactive' ).slideDown( 200 );
						composite.$composite_message.addClass( 'inactive' ).slideUp( 200 );

						if ( out_of_stock_found ) {
							composite.$composite_button.find( 'button' ).prop( 'disabled', true ).addClass( 'disabled' );
						} else {
							composite.$composite_button.find( 'button' ).prop( 'disabled', false ).removeClass( 'disabled' );
						}
					}

					composite_data.find( '.composite_wrap' ).trigger( 'wc-composite-show-add-to-cart' );

				} else {

					composite.disable_add_to_cart();
				}

				/**
				 * Update composite availability string
				 */

				$.each( composite.composite_components, function( index, component ) {
					if ( false === component.is_in_stock() ) {
						out_of_stock.push( wc_composite_params.i18n_insufficient_item_stock.replace( '%s', $( '#component_options_' + component.component_id + ' option:selected' ).data( 'title' ) ).replace( '%v', component.get_title() ) );
					}
				} );

				var $overridden_stock_status = false;

				// Build out-of-stock selections string

				if ( out_of_stock.length > 0 ) {

					var composite_out_of_stock_string = '<p class="stock out-of-stock">' + wc_composite_params.i18n_insufficient_stock + '</p>';

					var loop = 0;
					var out_of_stock_string = '';

					for ( var i in out_of_stock ) {

						loop++;

						if ( out_of_stock.length == 1 || loop == 1 ) {
							out_of_stock_string = out_of_stock[i];
						} else {
							out_of_stock_string = wc_composite_params.i18n_insufficient_item_stock_comma_sep.replace( '%s', out_of_stock_string ).replace( '%v', out_of_stock[i] );
						}
					}

					$overridden_stock_status = $( composite_out_of_stock_string.replace( '%s', out_of_stock_string ) );
				}

				var $current_stock_status = composite_data.find( '.composite_wrap p.stock' );

				if ( $overridden_stock_status ) {
					if ( $current_stock_status.length > 0 ) {
						if ( $current_stock_status.hasClass( 'inactive' ) ) {
							$current_stock_status.replaceWith( $overridden_stock_status.hide() );
							$overridden_stock_status.slideDown( 200 );
						} else {
							$current_stock_status.replaceWith( $overridden_stock_status );
						}
					} else {
						composite.$composite_button.before( $overridden_stock_status.hide() );
						$overridden_stock_status.slideDown( 200 );
					}
				} else {
					if ( composite.$composite_stock_status ) {
						$current_stock_status.replaceWith( composite.$composite_stock_status );
					} else {
						$current_stock_status.addClass( 'inactive' ).slideUp( 200 );
					}
				}

				/**
				 * Update summary widget
				 */

				composite.composite_summary_widget.update_price();
				composite.composite_summary_widget.update_error();

			},

			/**
			 * Called when the Composite can't be added-to-cart - disables the add-to-cart button and builds a string with a human-friendly reason
			 */

			disable_add_to_cart: function( hide_message ) {

				var composite        = this;
				var composite_data   = composite.$composite_data;

				if ( composite.composite_button_behaviour === 'new' ) {

					if ( typeof( hide_message ) === 'undefined' ) {
						var pending = composite.get_pending_components_string();
						if ( pending ) {
							hide_message = wc_composite_params.i18n_select_options.replace( '%s', pending );
						} else {
							hide_message = '';
						}
					}

					composite.$composite_price.addClass( 'inactive' ).slideUp( 200 );
					composite.$composite_message_content.html( hide_message );
					composite.$composite_message.removeClass( 'inactive' ).slideDown( 200 );
					composite.$composite_button.find( 'button' ).prop( 'disabled', true ).addClass( 'disabled' );

				} else {

					composite.$composite_price.html( '<p class="price"></p>' );
					composite_data.find( '.composite_wrap' ).slideUp( 200 );
				}

				composite_data.find( '.composite_wrap' ).trigger( 'wc-composite-hide-add-to-cart' );

			},

			/**
			 * Builds a string with all Components that require user input
			 */

			get_pending_components_string: function() {

				var composite                 = this;
				var pending_components        = [];
				var pending_components_string = '';

				$.each( composite.composite_components, function( index, component ) {

					var selection = component.get_selected_product_id();
					var item_set  = component.$component_data.data( 'component_set' );

					if ( ( ! ( selection > 0 ) && ! component.is_optional() ) || item_set == false || typeof( item_set ) === 'undefined' ) {
						pending_components.push( component.get_title() );
					}

				} );

				var count = pending_components.length;

				if ( count > 0 ) {

					var loop = 0;

					for ( var i in pending_components ) {

						loop++;

						if ( count == 1 || loop == 1 ) {
							pending_components_string = '&quot;' + pending_components[ i ] + '&quot;';
						} else if ( loop == count ) {
							pending_components_string = wc_composite_params.i18n_select_options_and_sep.replace( '%s', pending_components_string ).replace( '%v', pending_components[ i ] );
						} else {
							pending_components_string = wc_composite_params.i18n_select_options_comma_sep.replace( '%s', pending_components_string ).replace( '%v', pending_components[ i ] );
						}
					}
				}

				return pending_components_string;

			},

			/**
			 * Helper functions for updating the summary widget
			 */

			init_widget: function() {

				var composite = this;

				composite.composite_summary_widget = {

					$self:     composite.$composite_summary_widget,
					$elements: composite.$composite_summary_widget.find( '.widget_composite_summary_elements' ),
					$price:    composite.$composite_summary_widget.find( '.widget_composite_summary_price' ),
					$error:    composite.$composite_summary_widget.find( '.widget_composite_summary_error' ),

					bind_event_handlers: function() {

						/**
						 * On clicking a composite summary link (widget)
						 */
						composite.$composite_summary_widget

							.on( 'click', '.summary_element_link', function( event ) {

								var composite_summary = $(this).closest( '.composite_summary' );
								var container_id      = composite_summary.find( '.widget_composite_summary_content' ).data( 'container_id' );
								var form              = $( '#composite_data_' + container_id ).closest( '.composite_form' );

								if ( $(this).hasClass( 'disabled' ) ) {
									return false;
								}

								if ( composite.has_transition_lock ) {
									return false;
								}

								var step_id = $(this).closest( '.summary_element' ).data( 'item_id' );

								if ( typeof( step_id ) === 'undefined' ) {
									var element_index     = composite_summary.find( '.summary_element' ).index( $(this).closest( '.summary_element' ) );
									step_id               = form.find( '.multistep.component:eq(' + element_index + ')' ).data( 'item_id' );
								}

								var step = composite.get_step( step_id );

								if ( step === false ) {
									return false;
								}

								if ( step.get_element().hasClass( 'progressive' ) ) {
									step.block_next_steps();
								}

								if ( ! step.is_current() || composite.composite_layout === 'single' ) {
									step.show_step();
								}

								return false;
							} )

							.on( 'click', 'a.summary_element_tap', function( event ) {
								$(this).closest( '.summary_element_link' ).trigger( 'click' );
								return false;
							} );

					},

					update_markup: function() {

						var widget = this;

						if ( widget.$self.length > 0 ) {

							var clone = composite.$composite_summary.find( '.summary_elements' ).clone();

							clone.find( '.summary_element_wrapper' ).css( { 'height': 'auto' } );
							clone.find( '.summary_element' ).css( { 'width': '100%' } );
							clone.find( '.summary_element_selection_prompt' ).remove();

							widget.$elements.html( clone );
						}

					},

					update_links: function() {

						var widget = this;

						if ( composite.$composite_summary.length > 0 && widget.$self.length > 0 ) {

							$.each( composite.composite_steps, function( step_index, step ) {

								var summary_element      = widget.$self.find( '.summary_element_' + step.step_id );
								var summary_element_link = summary_element.find( '.summary_element_link' );

								summary_element_link.removeClass( 'disabled selected' );

								if ( step.view_elements.$summary_element_link.hasClass( 'disabled' ) ) {
									summary_element_link.addClass( 'disabled' );
								}

								if ( step.view_elements.$summary_element_link.hasClass( 'selected' ) ) {
									summary_element_link.addClass( 'selected' );
								}

							} );
						}

					},

					update_price: function() {

						var widget = this;

						if ( widget.$self.length > 0 ) {

							var price_clone = composite.$composite_price.clone();
							var html        = '';

							if ( ! price_clone.hasClass( 'inactive' ) ) {
								html = price_clone.html();
							}

							widget.$price.html( html );
						}

					},

					update_error: function() {

						var widget = this;

						if ( widget.$self.length > 0 ) {

							if ( composite.$composite_message.hasClass( 'inactive' ) ) {

								widget.$error.html( '' );
								widget.$error.removeClass( 'woocommerce-info' );

							} else {

								var error_clone = composite.$composite_message_content.clone();

								widget.$error.html( error_clone.html() );
								widget.$error.addClass( 'woocommerce-info' );
							}
						}
					}

				};

				composite.composite_summary_widget.bind_event_handlers();

			}

		};

		wc_cp_composite_scripts[ container_id ].init();
	};

	/**
	 * Construct a variable product selected attributes short description
	 */

	function wc_cp_get_variable_product_attributes_description( variations ) {

		var attribute_options        = variations.find( '.attribute-options' );
		var attribute_options_length = attribute_options.length;
		var meta                     = '';

		if ( attribute_options_length == 0 ) {
			return '';
		}

		attribute_options.each( function( index ) {

			var selected = $(this).find( 'select' ).val();

			if ( selected === '' ) {
				meta = '';
				return false;
			}

			meta = meta + $(this).data( 'attribute_label' ) + ': ' + $(this).find( 'select option:selected' ).text();

			if ( index !== attribute_options_length - 1 ) {
				meta = meta + ', ';
			}

		} );

		return meta;
	}

	/**
	 * Toggle-box handling
	 */

	function wc_cp_toggle_element( container, content ) {

		if ( container.hasClass( 'animating' ) ) {
			return false;
		}

		if ( container.hasClass( 'closed' ) ) {
			setTimeout( function() {
				content.slideDown( { duration: 200, queue: false, always: function() {
					container.removeClass( 'animating' );
				} } );
			}, 10 );
			container.removeClass( 'closed' ).addClass( 'open animating' );
		} else {
			content.slideUp( { duration: 200, queue: false } );
			container.removeClass( 'open' ).addClass( 'closed' );
		}

		return true;
	}

	/**
	 * Various helper functions
	 */

	function wc_cp_scroll_viewport( target, params ) {

		var partial;
		var offset_top;
		var offset_bottom;
		var timeout;
		var anim_duration;
		var anim_queue;
		var anim_complete;
		var always_complete;

		if ( typeof( params.partial ) === 'undefined' ) {
			partial = true;
		} else {
			partial = params.partial;
		}

		if ( typeof( params.offset_top ) === 'undefined' ) {
			offset_top = 50;
		} else {
			offset_top = params.offset_top;
		}

		if ( typeof( params.offset_bottom ) === 'undefined' ) {
			offset_bottom = 80;
		} else {
			offset_bottom = params.offset_bottom;
		}

		if ( typeof( params.timeout ) === 'undefined' ) {
			timeout = 5;
		} else {
			timeout = params.timeout;
		}

		if ( typeof( params.duration ) === 'undefined' ) {
			anim_duration = 250;
		} else {
			anim_duration = params.duration;
		}

		if ( typeof( params.queue ) === 'undefined' ) {
			anim_queue = false;
		} else {
			anim_queue = params.queue;
		}

		if ( typeof( params.on_complete ) === 'undefined' ) {
			anim_complete = function() {
				return false;
			};
		} else {
			anim_complete = params.on_complete;
		}

		if ( typeof( params.always_on_complete ) === 'undefined' ) {
			always_complete = false;
		} else {
			always_complete = params.always_on_complete;
		}

		if ( target.length > 0 && ! target.is_in_viewport( partial ) ) {

			var window_offset = target.hasClass( 'scroll_bottom' ) ? $(window).height() - offset_bottom : offset_top;

			// avoid scrolling both html and body
			var pos            = $( 'html' ).scrollTop();
			var animate_target = 'body';

			$( 'html' ).scrollTop( $( 'html' ).scrollTop() - 1 );
			if ( pos != $( 'html' ).scrollTop() ) {
				animate_target = 'html';
			}

			setTimeout( function() {
				$( animate_target ).animate( { scrollTop: target.offset().top - window_offset }, { duration: anim_duration, queue: anim_queue, always: anim_complete } );
			}, timeout );

		} else {
			if ( always_complete ) {
				anim_complete();
			}
		}
	}

	function wc_cp_woocommerce_number_format( price ) {

		var remove     = wc_composite_params.currency_format_decimal_sep;
		var position   = wc_composite_params.currency_position;
		var symbol     = wc_composite_params.currency_symbol;
		var trim_zeros = wc_composite_params.currency_format_trim_zeros;
		var decimals   = wc_composite_params.currency_format_num_decimals;

		if ( trim_zeros == 'yes' && decimals > 0 ) {
			for (var i = 0; i < decimals; i++) { remove = remove + '0'; }
			price = price.replace( remove, '' );
		}

		var price_format = '';

		if ( position == 'left' ) {
			price_format = '<span class="amount">' + symbol + price + '</span>';
		} else if ( position == 'right' ) {
			price_format = '<span class="amount">' + price + symbol +  '</span>';
		} else if ( position == 'left_space' ) {
			price_format = '<span class="amount">' + symbol + ' ' + price + '</span>';
		} else if ( position == 'right_space' ) {
			price_format = '<span class="amount">' + price + ' ' + symbol +  '</span>';
		}

		return price_format;
	}

	function wc_cp_number_format( number ) {

		var decimals      = wc_composite_params.currency_format_num_decimals;
		var decimal_sep   = wc_composite_params.currency_format_decimal_sep;
		var thousands_sep = wc_composite_params.currency_format_thousand_sep;

	    var n = number, c = isNaN( decimals = Math.abs( decimals ) ) ? 2 : decimals;
	    var d = typeof( decimal_sep ) === 'undefined' ? ',' : decimal_sep;
	    var t = typeof( thousands_sep ) === 'undefined' ? '.' : thousands_sep, s = n < 0 ? '-' : '';
	    var i = parseInt( n = Math.abs( +n || 0 ).toFixed(c) ) + '', j = ( j = i.length ) > 3 ? j % 3 : 0;

	    return s + ( j ? i.substr( 0, j ) + t : '' ) + i.substr(j).replace( /(\d{3})(?=\d)/g, '$1' + t ) + ( c ? d + Math.abs( n - i ).toFixed(c).slice(2) : '' );
	}

	function wc_cp_intersect_safe( a, b ) {

		var ai     = 0, bi = 0;
		var result = [];

		a.sort();
		b.sort();

		while ( ai < a.length && bi < b.length ) {

			if ( a[ai] < b[bi] ) {
				ai++;
			} else if ( a[ai] > b[bi] ) {
				bi++;
			/* they're equal */
			} else {
				result.push( a[ai] );
				ai++;
				bi++;
			}
		}

		return result;
	}

	function wc_cp_log( message ) {

		if ( window.console ) {
			window.console.log( message );
		}
	}

    $.fn.is_in_viewport = function( partial, hidden, direction ) {

    	var $w = $( window );

        if ( this.length < 1 ) {
            return;
        }

        var $t         = this.length > 1 ? this.eq(0) : this,
			t          = $t.get(0),
			vpWidth    = $w.width(),
			vpHeight   = $w.height(),
			direction  = (direction) ? direction : 'both',
			clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

        if (typeof t.getBoundingClientRect === 'function'){

            // Use this native browser method, if available.
            var rec = t.getBoundingClientRect(),
                tViz = rec.top    >= 0 && rec.top    <  vpHeight,
                bViz = rec.bottom >  0 && rec.bottom <= vpHeight,
                lViz = rec.left   >= 0 && rec.left   <  vpWidth,
                rViz = rec.right  >  0 && rec.right  <= vpWidth,
                vVisible   = partial ? tViz || bViz : tViz && bViz,
                hVisible   = partial ? lViz || rViz : lViz && rViz;

            if ( direction === 'both' ) {
                return clientSize && vVisible && hVisible;
            } else if ( direction === 'vertical' ) {
                return clientSize && vVisible;
            } else if ( direction === 'horizontal' ) {
                return clientSize && hVisible;
            }

        } else {

            var viewTop         = $w.scrollTop(),
                viewBottom      = viewTop + vpHeight,
                viewLeft        = $w.scrollLeft(),
                viewRight       = viewLeft + vpWidth,
                offset          = $t.offset(),
                _top            = offset.top,
                _bottom         = _top + $t.height(),
                _left           = offset.left,
                _right          = _left + $t.width(),
                compareTop      = partial === true ? _bottom : _top,
                compareBottom   = partial === true ? _top : _bottom,
                compareLeft     = partial === true ? _right : _left,
                compareRight    = partial === true ? _left : _right;

            if ( direction === 'both' ) {
                return !!clientSize && ( ( compareBottom <= viewBottom ) && ( compareTop >= viewTop ) ) && ( ( compareRight <= viewRight ) && ( compareLeft >= viewLeft ) );
            } else if ( direction === 'vertical' ) {
                return !!clientSize && ( ( compareBottom <= viewBottom ) && ( compareTop >= viewTop ) );
            } else if ( direction === 'horizontal' ) {
                return !!clientSize && ( ( compareRight <= viewRight ) && ( compareLeft >= viewLeft ) );
            }
        }
    };

    /*
     * Step class
     */

     function WC_CP_Step( composite, $step, index ) {

     	var step_id        = $step.data( 'item_id' );

		this.step_id       = step_id;
		this.step_index    = index;
		this.step_title    = $step.data( 'nav_title' );
		this._is_component = $step.hasClass( 'component' );
		this._is_review    = $step.hasClass( 'cart' );

		this.$self         = $step;

		this.view_elements = {

			$summary_element:         composite.$composite_summary.find( '.summary_element_' + step_id ),
			$summary_element_link:    composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_link' ),

			$summary_element_wrapper: composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_wrapper' ),
			$summary_element_inner:   composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_wrapper_inner' ),

			$summary_element_title:   composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_selection' ),
			$summary_element_image:   composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_image' ),
			$summary_element_price:   composite.$composite_summary.find( '.summary_element_' + step_id + ' .summary_element_price' ),

			$pagination_element:      composite.$composite_pagination.find( '.pagination_element_' + step_id ),
			$pagination_element_link: composite.$composite_pagination.find( '.pagination_element_' + step_id + ' .element_link' ),

		};

		this.get_title = function() {

			return this.step_title;

		};

		this.get_element = function() {

			return this.$self;

		};

		this.is_review = function() {

			return this._is_review;

		};

		this.is_component = function() {

			return this._is_component;

		};

		this.get_component = function() {

			if ( this._is_component ) {
				return composite.composite_components[ this.step_index ];
			} else {
				return false;
			}
		};

		this.is_current = function() {

			return this.$self.hasClass( 'active' );

		};

		this.is_next = function() {

			return this.$self.hasClass( 'next' );

		};

		this.is_previous = function() {

			return this.$self.hasClass( 'prev' );

		};

		/**
			 * Brings a new step into view - called when clicking on a navigation element
		 */

		this.show_step = function() {

			var step            = this;
			var form            = composite.$composite_form;
			var style           = composite.composite_layout;
			var style_variation = composite.composite_layout_variation;
			var item            = this.$self;
			var do_scroll       = composite.composite_initialized === false ? false : true;

			// scroll to the desired section
			if ( style === 'single' && do_scroll ) {

				wc_cp_scroll_viewport( item, { partial: false, duration: 250, queue: false } );

				// no more work when using the stacked layout
				return false;
			}

			if ( style === 'paged' && do_scroll ) {

				wc_cp_scroll_viewport( form.find( '.scroll_show_component' ), { timeout: 20, partial: true, duration: 250, queue: false } );

				setTimeout( function() {

					// fade out or show summary widget
					if ( composite.$composite_summary_widget.length > 0 ) {
						if ( step.is_review() ) {
							composite.$composite_summary_widget.animate( { opacity: 0 }, { duration: 250, queue: false } );
						} else {
							composite.$composite_summary_widget.slideDown( 250 );
							composite.$composite_summary_widget.animate( { opacity: 1 }, { duration: 250, queue: false } );
						}
					}

					// move summary widget out of the way if needed
					if ( step.is_review() ) {
						composite.$composite_summary_widget.slideUp( 250 );
					}

				}, 20 );

				if ( style_variation === 'componentized' ) {
					if ( step.is_review() ) {
						composite.$composite_navigation.css( { visibility: 'hidden' } );
					} else {
						composite.$composite_navigation.css( { visibility: 'visible' } );
					}
				}

			}

			// move active component
			step.set_active();

			// update blocks
			step.update_block_state();

			// update selections
			step.fire_scenario_actions();

			// autoload more results if all loaded thumbnail options are hidden
			if ( wc_composite_params.no_compat_options_autoload === 'yes' ) {
				if ( style !== 'single' && composite.composite_selection_mode === 'thumbnails' && step.is_component() ) {

					var component = step.get_component();

					if ( component.append_results() && component.hide_disabled_products() ) {

						var load_more        = component.$component_pagination.find( '.component_options_load_more' );
						var results_per_page = load_more.data( 'results_per_page' );

						if ( false === component.has_compat_results || component.compat_results_count < results_per_page ) {
							setTimeout( function() {
								load_more.trigger( 'click' );
							}, 300 );
						}
					}
				}
			}

			// update ui
			composite.update_ui( true );

			// scroll to the desired section (progressive)
			if ( style === 'progressive' && do_scroll && item.hasClass( 'autoscrolled' ) ) {

				wc_cp_scroll_viewport( item, { timeout: 250, partial: false, duration: 250, queue: false } );
			}

			item.trigger( 'wc-composite-show-component' );

		};

		/**
		 * Sets a step as active by hiding the previous one and updating the steps' markup
		 */

		this.set_active = function() {

			var step            = this;
			var form            = composite.$composite_form;
			var style           = composite.composite_layout;
			var style_variation = composite.composite_layout_variation;
			var active_step     = composite.get_current_step();
			var is_active       = false;

			if ( active_step.step_id == step.step_id ) {
				is_active = true;
			}

			composite.last_active_step_index = active_step.step_index;

			form.children( '.multistep.active, .multistep.next, .multistep.prev' ).removeClass( 'active next prev' );

			this.get_element().addClass( 'active' );

			var next_item = this.get_element().next();
			var prev_item = this.get_element().prev();

			if ( style === 'paged' && style_variation === 'componentized' ) {
				next_item = form.find( '.multistep.cart' );
				prev_item = form.find( '.multistep.cart' );
			}

			if ( next_item.hasClass( 'multistep' ) ) {
				next_item.addClass( 'next' );
			}

			if ( prev_item.hasClass( 'multistep' ) ) {
				prev_item.addClass( 'prev' );
			}

			if ( style !== 'progressive' ) {

				if ( ! is_active ) {

					composite.has_transition_lock = true;

					if ( wc_composite_params.transition_type === 'slide' ) {

						setTimeout( function() {

							var active_step_height = active_step.get_element().height();
							var step_height        = step.get_element().height();
							var max_height         = Math.max( active_step_height, step_height );
							var ref_duration       = 150 + Math.min( 450, parseInt( max_height / 5 ) );

							// hide with a sliding effect
							active_step.get_element().addClass( 'faded' ).slideUp( { duration: ref_duration, always: function() {
								active_step.get_element().removeClass( 'faded' );
							} } );

							// show with a sliding effect
							if ( active_step.step_index < step.step_index ) {
								step.get_element().after( '<div style="display:none" class="active_placeholder"></div>' );
								step.get_element().insertBefore( active_step.get_element() );
							}

							step.get_element().slideDown( { duration: ref_duration, always: function() {

								if ( active_step.step_index < step.step_index ) {
									composite.$composite_form.find( '.active_placeholder' ).replaceWith( step.get_element() );
								}

								composite.has_transition_lock = false;

							} } );

						}, 150 );

					} else if ( wc_composite_params.transition_type === 'fade' ) {

						// fadeout
						active_step.get_element().addClass( 'faded' );

						step.get_element().addClass( 'faded' );

						setTimeout( function() {

							// show new
							step.get_element().show();
							// hide old
							active_step.get_element().hide();
							// fade in new
							setTimeout( function() {
								step.get_element().removeClass( 'faded' );
							}, 20 );

							composite.has_transition_lock = false;

						}, 300 );
					}

				} else {
					step.get_element().show();
				}
			}

			this.$self.trigger( 'wc-composite-set-active-component' );

		};

		/**
		 * Updates the block state of a progressive step that's brought into view
		 */

		this.update_block_state = function() {

			var style = composite.composite_layout;

			if ( style !== 'progressive' ) {
				return false;
			}

			var prev_step = composite.get_previous_step();

			if ( prev_step !== false ) {
				prev_step.block_step_inputs();
				prev_step.toggle_step( 'closed', true );
			}

			this.unblock_step_inputs();
			this.unblock_step();

			if ( prev_step !== false ) {
				var reset_options = prev_step.get_element().find( '.clear_component_options' );
				reset_options.html( wc_composite_params.i18n_reset_selection ).addClass( 'reset_component_options' );
			}

		};

		/**
		 * Unblocks access to step in progressive mode
		 */

		this.unblock_step = function() {

			this.toggle_step( 'open', true );

			this.$self.removeClass( 'blocked' );

		};

		/**
		 * Blocks access to all later steps in progressive mode
		 */

		this.block_next_steps = function() {

			var min_block_index = this.step_index;

			$.each( composite.composite_steps, function( index, step ) {

				if ( index > min_block_index ) {

					if ( step.get_element().hasClass( 'disabled' ) ) {
						step.unblock_step_inputs();
					}

					step.block_step();
				}
			} );

		};

		/**
		 * Blocks access to step in progressive mode
		 */

		this.block_step = function() {

			this.$self.addClass( 'blocked' );

			this.toggle_step( 'closed', false );

		};

		/**
		 * Toggle step in progressive mode
		 */

		this.toggle_step = function( state, active ) {

			if ( this.$self.hasClass( 'toggled' ) ) {

				if ( state === 'open' ) {
					if ( this.$self.hasClass( 'closed' ) ) {
						wc_cp_toggle_element( this.$self, this.$self.find( '.component_inner' ) );
					}

				} else if ( state === 'closed' ) {
					if ( this.$self.hasClass( 'open' ) ) {
						wc_cp_toggle_element( this.$self, this.$self.find( '.component_inner' ) );
					}
				}

				if ( active ) {
					this.$self.find( '.component_title' ).removeClass( 'inactive' );
				} else {
					this.$self.find( '.component_title' ).addClass( 'inactive' );
				}
			}

		};

		/**
		 * Unblocks step inputs
		 */

		this.unblock_step_inputs = function() {

			this.$self.find( 'select.disabled_input, input.disabled_input' ).removeClass( 'disabled_input' ).prop( 'disabled', false );

			this.$self.removeClass( 'disabled' ).trigger( 'wc-composite-enable-component-options' );

			var reset_options = this.$self.find( '.clear_component_options' );

			reset_options.html( wc_composite_params.i18n_clear_selection ).removeClass( 'reset_component_options' );

		};

		/**
		 * Blocks step inputs
		 */

		this.block_step_inputs = function() {

			this.$self.find( 'select, input' ).addClass( 'disabled_input' ).prop( 'disabled', 'disabled' );

			this.$self.addClass( 'disabled' ).trigger( 'wc-composite-disable-component-options' );

			var reset_options = this.$self.find( '.clear_component_options' );

			reset_options.html( wc_composite_params.i18n_reset_selection ).addClass( 'reset_component_options' );

		};

		/**
		 * True if access to the step is blocked (progressive mode)
		 */

		this.is_blocked = function() {

			return this.$self.hasClass( 'blocked' );

		};

		/**
		 * True if access to the step inputs is blocked (progressive mode)
		 */

		this.has_blocked_inputs = function() {

			return this.$self.hasClass( 'disabled' );

		};

		/**
		 * Fire state actions based on scenarios
		 */

		this.fire_scenario_actions = function( excl_firing ) {

			if ( composite.has_scenarios_update_lock ) {
				return false;
			}

			if ( typeof( excl_firing ) === 'undefined' ) {
				excl_firing = false;
			}

			composite.actions_nesting++;

			// Update active scenarios
			composite.update_active_scenarios( this.step_id );

			// Update selections - 'compat_group' scenario action
			composite.update_selections( this.step_id, excl_firing );

			if ( composite.actions_nesting === 1 ) {

				// Signal 3rd party scripts to fire their own actions
				this.get_element().trigger( 'wc-composite-fire-scenario-actions' );
			}

			composite.actions_nesting--;

		};

		/**
		 * Validate step inputs
		 * True if a component has been fully configured - i.e. product/variation selected and in stock
		 */

		this.validate_inputs = function() {

			if ( this.is_component() ) {

				var component = this.get_component();

				var product_id   = component.get_selected_product_id();
				var product_type = component.get_selected_product_type();

				if ( product_id > 0 && component.is_in_stock() ) {

					if ( product_type === 'variable' ) {

						if ( component.$component_summary.find( '.variations_button input[name="variation_id"]' ).val() != '' ) {
							return true;
						} else {
							return false;
						}

					}  else if ( product_type === 'simple' || product_type === 'none' ) {

						return true;

					} else {

						if ( component.$component_data.data( 'component_set' ) == true ) {
							return true;
						} else {
							return false;
						}
					}

				} else if ( product_id === '' && component.is_optional() ) {

					return true;

				} else {

					return false;
				}
			}

			return false;

		};

	}
    /*
     * Component class
     */

	function WC_CP_Component( composite, $component, index ) {

		this.component_index           = index;
		this.component_id              = $component.attr( 'data-item_id' );
		this.component_title           = $component.data( 'nav_title' );

		this._is_optional              = false;
		this._is_configured            = false;

		this.has_compat_results        = true;
		this.compat_results_count      = 0;

		this._hide_disabled_products   = $component.hasClass( 'hide-incompatible-products' );
		this._hide_disabled_variations = $component.hasClass( 'hide-incompatible-variations' );
		this._append_results           = $component.hasClass( 'append-results' );

		this._selection_id             = false;
		this._selection_invalid        = false;

		this.initial_selection_id      = $component.find( 'select.component_options_select' ).val();

		this.$self                     = $component;
		this.$component_summary        = $component.find( '.component_summary' );
		this.$component_selections     = $component.find( '.component_selections' );
		this.$component_content        = $component.find( '.component_content' );
		this.$component_options        = $component.find( '.component_options' );
		this.$component_options_inner  = $component.find( '.component_options_inner' );
		this.$component_inner          = $component.find( '.component_inner' );
		this.$component_pagination     = $component.find( '.component_pagination' );

		this.$component_data           = $();

		/**
		 * True when component options are appended using a 'load more' button, instead of paginated
		 */

		this.append_results = function() {

			return this._append_results;
		};

		/**
		 * Set the selected product id
		 */

		this.set_selection_id = function( val ) {

			this._selection_id = val;

		};

		/**
		 * Get the selected product id
		 */

		this.get_selection_id = function() {

			return this._selection_id;

		};

		/**
		 * Set a selection as invalid when it is currently disabled
		 */

		this.set_selection_invalid = function( status ) {

			this._selection_invalid = status;

		};

		/**
		 * When true, hide incompatible/disabled products
		 */

		this.hide_disabled_products = function() {

			return this._hide_disabled_products;

		};

		/**
		 * When true, hide incompatible/disabled variations
		 */

		this.hide_disabled_variations = function() {

			return this._hide_disabled_variations;

		};

		/**
		 * Get the product type of the selected product
		 */

		this.get_selected_product_type = function() {

			return this.$component_data.data( 'product_type' );

		};

		/**
		 * Get the product id of the selected product (non casted)
		 */

		this.get_selected_product_id = function() {

			if ( this._selection_invalid ) {
				return null;
			}

			return this.$component_options.find( '#component_options_' + this.component_id ).val();

		};

		/**
		 * True if the component has an out-of-stock availability class
		 */

		this.is_in_stock = function() {

			if ( this.$component_summary.find( '.component_wrap .out-of-stock' ).not( '.inactive' ).length > 0 ) {
				return false;
			}

			return true;

		};

		/**
		 * True if the component is configured and ready to be purchased
		 */

		this.is_configured = function() {

			return this._is_configured;

		};

		/**
		 * Sets _is_configured to true if the component is configured and ready to be purchased
		 * Status saved during final validation and totals calculation in update_composite()
		 */

		this.set_configured = function( status ) {

			this._is_configured = status;

		};

		/**
		 * Initialize component scripts dependent on product type - called when selecting a new Component Option
		 * When called with init = false, no type-dependent scripts will be initialized
		 */

		this.init_scripts = function( init ) {

			if ( typeof( init ) === 'undefined' ) {
				init = true;
			}

			this.$component_data = this.$self.find( '.component_data' );

			if ( init ) {

				var product_type    = this.get_selected_product_type();
				var summary_content = this.$self.find( '.component_summary > .content' );

				if ( product_type === 'variable' ) {

					if ( ! summary_content.hasClass( 'cart' ) ) {
						summary_content.addClass( 'cart' );
					}

					if ( ! summary_content.hasClass( 'variations_form' ) ) {
						summary_content.addClass( 'variations_form' );
					}

					// Put filtered variations in place
					summary_content.data( 'product_variations', this.$component_data.data( 'product_variations' ) );

					// Initialize variations script
					summary_content.wc_variation_form();

					// Fire change in order to save 'variation_id' input
					summary_content.find( '.variations select' ).change();

				} else if ( product_type === 'bundle' ) {

					if ( ! summary_content.hasClass( 'bundle_form' ) ) {
						summary_content.addClass( 'bundle_form' );
					}

					// Initialize bundles script now
					summary_content.find( '.bundle_data' ).wc_pb_bundle_form();

				} else {

					if ( ! summary_content.hasClass( 'cart' ) ) {
						summary_content.addClass( 'cart' );
					}
				}
			}

		};

		/**
		 * Get the step that corresponds to this component
		 */

		this.get_step = function() {

			return composite.get_step( this.component_id );

		};

		/**
		 * Get the title of this component
		 */

		this.get_title = function() {

			return this.component_title;

		};

		/**
		 * Add active/filtered classes to the component filters markup, can be used for styling purposes
		 */

		this.update_filters_ui = function() {

			var component_filters = this.$self.find( '.component_filters' );
			var filters           = component_filters.find( '.component_filter' );
			var all_empty         = true;

			if ( filters.length == 0 ) {
				return false;
			}

			filters.each( function() {

				if ( $(this).find( '.component_filter_option.selected' ).length == 0 ) {
					$(this).removeClass( 'active' );
				} else {
					$(this).addClass( 'active' );
					all_empty = false;
				}

			} );

			if ( all_empty ) {
				component_filters.removeClass( 'filtered' );
			} else {
				component_filters.addClass( 'filtered' );
			}
		};

		/**
		 * Collect active component filters and options and build an object for posting
		 */

		this.get_active_filters = function() {

			var component_filters = this.$self.find( '.component_filters' );
			var filters           = {};

			if ( component_filters.length == 0 ) {
				return filters;
			}

			component_filters.find( '.component_filter_option.selected' ).each( function() {

				var filter_type = $(this).closest( '.component_filter' ).data( 'filter_type' );
				var filter_id   = $(this).closest( '.component_filter' ).data( 'filter_id' );
				var option_id   = $(this).data( 'option_id' );

				if ( filter_type in filters ) {

					if ( filter_id in filters[ filter_type ] ) {

						filters[ filter_type ][ filter_id ].push( option_id );

					} else {

						filters[ filter_type ][ filter_id ] = [];
						filters[ filter_type ][ filter_id ].push( option_id );
					}

				} else {

					filters[ filter_type ]              = {};
					filters[ filter_type ][ filter_id ] = [];
					filters[ filter_type ][ filter_id ].push( option_id );
				}

			} );

			return filters;
		};

		/**
		 * Append more component options via ajax - called upon sorting, updating filters, or viewing a new page
		 */

		this.append_component_options = function( data ) {
			this.reload_component_options( data, true );
		};

		/**
		 * Update the available component options via ajax - called upon sorting, updating filters, or viewing a new page
		 */

		this.reload_component_options = function( data, appending_results ) {

			var component               = this;
			var item                    = this.$self;
			var component_selections    = this.$component_selections;
			var component_options       = this.$component_options;
			var component_options_inner = this.$component_options_inner;
			var component_pagination    = this.$component_pagination;
			var load_height             = component_options.outerHeight();
			var new_height              = 0;
			var animate_height          = false;
			var reload                  = false;
			var delay                   = 250;

			var ajax_url                = wc_composite_params.use_wc_ajax === 'yes' ? composite.ajax_url.toString().replace( '%%endpoint%%', 'woocommerce_show_component_options' ) : composite.ajax_url;

			// Do nothing if the component is disabled
			if ( item.hasClass( 'disabled' ) ) {
				return false;
			}

			if ( typeof( appending_results ) === 'undefined' ) {
				appending_results = false;
			}

			var animate_component_options = function() {

				// animate component options container
				if ( animate_height ) {

					component_options.animate( { 'height' : new_height }, { duration: 200, queue: false, always: function() {
						component_options.css( { 'height' : 'auto' } );
						component_selections.unblock().removeClass( 'blocked_content refresh_component_options' );
					} } );

				} else {
					component_selections.unblock().removeClass( 'blocked_content refresh_component_options' );
				}

			};

			// block container
			component_selections.addClass( 'blocked_content' ).block( wc_cp_block_params );

			// no wait for animations while reloading
			if ( composite.append_results_nesting > 0 ) {
				delay = 5;
			}

			setTimeout( function() {

				// get product info via ajax
				$.post( ajax_url, data, function( response ) {

					// fade thumbnails when reloading results
					if ( ! appending_results ) {
						component_selections.addClass( 'refresh_component_options' );
					}

					setTimeout( function() {

						try {

							// lock height
							component_options.css( 'height', load_height );

							if ( response.result === 'success' ) {

								var component_options_select = component_options.find( 'select.component_options_select' );
								var current_selection_id     = component_options_select.val();

								var response_markup          = $( response.options_markup );

								var thumbnails_container     = component_options_inner.find( '.component_option_thumbnails_container' );
								var new_thumbnail_options    = response_markup.find( '.component_option_thumbnail_container' );

								if ( component.append_results() ) {
									new_thumbnail_options.addClass( 'new' );
								}

								// put new content in place
								if ( appending_results ) {

									// reset select options
									if ( typeof( component_options_select.data( 'select_options' ) ) !== 'undefined' && component_options_select.data( 'select_options' ) !== false ) {
										component_options_select.find( 'option:gt(0)' ).remove();
										component_options_select.append( component_options_select.data( 'select_options' ) );
										component_options_select.find( 'option:gt(0)' ).removeClass( 'disabled' );
										component_options_select.find( 'option:gt(0)' ).removeAttr( 'disabled' );
									}

									// appending product thumbnails...
									var new_select_options      = response_markup.find( 'select.component_options_select option' );
									var default_selected_option = component_options_select.find( 'option[value="' + component.initial_selection_id + '"]' );

									// clean up and merge the existing + newly loaded select options
									new_select_options = new_select_options.filter( ':gt(0)' );

									if ( component.initial_selection_id > 0 && thumbnails_container.find( '#component_option_thumbnail_' + component.initial_selection_id ).length == 0 ) {

										default_selected_option.remove();

										if ( current_selection_id > 0 && thumbnails_container.find( '#component_option_thumbnail_' + current_selection_id ).length > 0 ) {
											new_select_options = new_select_options.not( ':selected' );
										}

									} else {

										new_select_options = new_select_options.not( ':selected' );
									}

									new_select_options.appendTo( component_options_select );

									// append thumbnails
									new_thumbnail_options.appendTo( thumbnails_container );

								} else {

									// reloading product thumbnails...

									component_options_inner.html( $( response_markup ).find( '.component_options_inner' ).html() );

									component.initial_selection_id = component_options_select.val();

								}

								var thumbnail_images = component_options_inner.find( '.component_option_thumbnail_container:not(.hidden) img' );

								// preload images before proceeding
								var preload_images_then_show_component_options = function() {

									if ( thumbnail_images.length > 0 ) {

										var retry = false;

										thumbnail_images.each( function() {

											var image = $(this);

											if ( image.height() === 0 ) {
												retry = true;
												return false;
											}

										} );

										if ( retry ) {
											setTimeout( function() {
												preload_images_then_show_component_options();
											}, 100 );
										} else {
											show_component_options();
										}
									} else {
										show_component_options();
									}
								};

								var show_component_options = function() {

									var pages_left       = 0;
									var results_per_page = 0;
									var pages_loaded     = 0;
									var pages            = 0;
									var load_more;

									// update pagination
									if ( response.pagination_markup ) {

										component_pagination.html( $( response.pagination_markup ).html() );

										if ( component.append_results() ) {

											load_more        = component_pagination.find( '.component_options_load_more' );
											pages_loaded     = load_more.data( 'pages_loaded' );
											pages            = load_more.data( 'pages' );
											results_per_page = load_more.data( 'results_per_page' );

											pages_left       = pages - pages_loaded;
										}

										component_pagination.slideDown( 200 );

									} else {

										if ( component.append_results() ) {

											load_more        = component_pagination.find( '.component_options_load_more' );
											results_per_page = load_more.data( 'results_per_page' );

											load_more.data( 'pages_loaded', load_more.data( 'pages' ) );
										}

										component_pagination.slideUp( 200 );
									}

									// reset options
									component_options_select.data( 'select_options', false );

									// update component scenarios with new data
									var scenario_data = composite.$composite_data.data( 'scenario_data' );

									if ( appending_results ) {

										// append product scenario data
										$.each( response.component_scenario_data, function( product_id, product_in_scenarios ) {
											scenario_data.scenario_data[ data.component_id ][ product_id ] = product_in_scenarios;
										} );

									} else {

										// replace product scenario data
										scenario_data.scenario_data[ data.component_id ] = response.component_scenario_data;
									}

									// if the initial selection is not part of the result set, reset
									// should never happen - in thumbnails mode, the initial selection is always appended to the (hidden) dropdown
									var initial_selection_id = current_selection_id;
									current_selection_id     = component_options_select.val();

									if ( initial_selection_id > 0 && ( current_selection_id === '' || typeof( current_selection_id ) === 'undefined' ) ) {

										component_options_select.change();

									} else {

										// disable newly loaded products and variations
										component.get_step().fire_scenario_actions();

										// count how many of the newly loaded results are actually visible
										if ( component.append_results() && component.hide_disabled_products() ) {

											var newly_added_thumbnails = thumbnails_container.find( '.component_option_thumbnail_container.new' );
											var of_which_visible       = newly_added_thumbnails.not( '.hidden' );

											newly_added_thumbnails.removeClass( 'new' );

											composite.append_results_nesting_count += of_which_visible.length;

											if ( composite.append_results_nesting_count < results_per_page && pages_left > 0 ) {

												composite.append_results_nesting++;
												reload = true;

												if ( composite.append_results_nesting > 10 ) {
													if ( window.confirm( wc_composite_params.i18n_reload_threshold_exceeded.replace( '%s', component.get_title() ) ) ) {
														composite.append_results_nesting = 0;
													} else {
														reload = false;
													}
												}
											}
										}

										// update ui
										if ( ! reload ) {

											composite.append_results_nesting_count = 0;
											composite.append_results_nesting       = 0;

											composite.update_ui();
										}
									}

									if ( ! reload ) {

										item.trigger( 'wc-composite-component-options-loaded' );

										// measure height
										new_height = component_options_inner.outerHeight( true );

										if ( Math.abs( new_height - load_height ) > 1 ) {
											animate_height = true;
										} else {
											component_options.css( 'height', 'auto' );
										}

										animate_component_options();

									} else {

										data.load_page++;

										component.append_component_options( data );
									}
								};

								if ( ! reload ) {
									preload_images_then_show_component_options();
								}

							} else {

								// show failure message
								component_options_inner.html( response.options_markup );

								// measure height
								new_height = component_options_inner.outerHeight( true );

								if ( Math.abs( new_height - load_height ) > 1 ) {
									animate_height = true;
								} else {
									component_options.css( 'height', 'auto' );
								}

								animate_component_options();
							}

						} catch ( err ) {

							// show failure message
							wc_cp_log( err );

							composite.append_results_nesting_count = 0;
							composite.append_results_nesting       = 0;

							animate_component_options();
						}

					}, delay );

				}, 'json' );

			}, delay );

		};

		/*
		 * Scripts run after selecting a new Component Option
		 */

		this.updated_selection = function() {

			var component                = this;
			var step                     = component.get_step();

			var component_options_select = component.$component_options.find( 'select.component_options_select' );
			component.set_selection_id( component_options_select.val() );

			step.fire_scenario_actions();

			composite.update_ui();
			composite.update_composite();

		};

		/**
		 * Update the available component options via ajax - called upon sorting, updating filters, or viewing a new page
		 */

		this.select_component_option = function( data ) {

			var component            = this;
			var component_selections = component.$component_selections;
			var component_content    = component.$component_content;
			var component_summary    = component.$component_summary;
			var summary_content      = component.$self.find( '.component_summary > .content' );
			var form                 = composite.$composite_form;
			var style                = composite.composite_layout;
			var scroll_to            = '';

			var load_height          = component_summary.outerHeight( true );
			var new_height           = 0;
			var animate_height       = false;

			var ajax_url             = wc_composite_params.use_wc_ajax === 'yes' ? composite.ajax_url.toString().replace( '%%endpoint%%', 'woocommerce_show_composited_product' ) : composite.ajax_url;

			if ( style === 'paged' ) {
				scroll_to = form.find( '.scroll_select_component_option' );
			} else {
				scroll_to = component_content;
			}

			if ( data.product_id !== '' ) {

				// block component selections
				component_selections.addClass( 'blocked_content' ).block( wc_cp_block_params );

				// block composite transitions
				composite.has_transition_lock = true;

				// get product info via ajax
				$.post( ajax_url, data, function( response ) {

					try {

						// lock height
						component_content.css( 'height', load_height );

						// put content in place
						summary_content.html( response.markup );

						// measure height
						new_height = component_summary.outerHeight( true );

						if ( Math.abs( new_height - load_height ) > 1 ) {
							animate_height = true;
						} else {
							component_content.css( 'height', 'auto' );
						}

						if ( response.result === 'success' ) {

							component.init_qty_input();
							component.init_scripts();
							component.updated_selection();

							component.$self.trigger( 'wc-composite-component-loaded' );

						} else {

							component.init_scripts( false );
							component.updated_selection();
						}

					} catch ( err ) {

						// show failure message
						wc_cp_log( err );

						// reset content
						summary_content.html( '<div class="component_data" data-component_set="true" data-price="0" data-regular_price="0" data-product_type="none" style="display:none;"></div>' );

						component.init_scripts( false );
						component.updated_selection();

					}

					// animate component content height and scroll to selected product details
					setTimeout( function() {

						if ( animate_height ) {

							// re-measure height to account for animations in loaded markup
							new_height = component_summary.outerHeight( true );

							if ( Math.abs( new_height - load_height ) > 1 ) {
								animate_height = true;
							}

							// animate component content height
							component_content.animate( { 'height': new_height }, { duration: 200, queue: false, always: function() {

								// scroll
								var top_offset    = style === 'paged' ? 50 : $(window).height() - new_height - 70;
								var bottom_offset = 20;

								wc_cp_scroll_viewport( scroll_to, { offset_top: top_offset, offset_bottom: bottom_offset, partial: false, duration: 200, queue: false, always_on_complete: true, on_complete: function() {

									// reset height
									component_content.css( { 'height' : 'auto' } );

									// unblock component
									component_selections.unblock().removeClass( 'blocked_content' );
									composite.has_transition_lock = false;

								} } );

							} } );

						} else {

							// scroll
							var top_offset    = style === 'paged' ? 50 : $(window).height() - new_height - 70;
							var bottom_offset = 20;

							wc_cp_scroll_viewport( scroll_to, { offset_top: top_offset, offset_bottom: bottom_offset, partial: false, duration: 200, queue: false, always_on_complete: true, on_complete: function() {

								// unblock component
								component_selections.unblock().removeClass( 'blocked_content' );
								composite.has_transition_lock = false;

							} } );
						}

					}, 400 );

				}, 'json' );

			} else {

				var animate = true;

				if ( component.$self.hasClass( 'resetting' ) && component.$self.hasClass( 'toggled' ) ) {
					animate = false;
				} else {
					// lock height
					component_content.css( 'height', load_height );
				}

				// reset content
				summary_content.html( '<div class="component_data" data-component_set="true" data-price="0" data-regular_price="0" data-product_type="none" style="display:none;"></div>' );

				component.init_scripts( false );
				component.updated_selection();

				if ( animate ) {
					// animate component content height
					component_content.animate( { 'height': component_summary.outerHeight( true ) }, { duration: 200, queue: false, always: function() {
						component_content.css( { 'height': 'auto' } );
					} } );
				}
			}
		};

		/**
		 * True if a Component is set as optional
		 */

		this.is_optional = function() {

			return this._is_optional;

		};

		/**
		 * Set Component as optional
		 */

		this.set_optional = function( optional ) {

			if ( optional && ! this._is_optional ) {
				this.$component_selections.find( 'option.none' ).html( wc_composite_params.i18n_none );
			} else if ( ! optional && this._is_optional ) {
				this.$component_selections.find( 'option.none' ).html( wc_composite_params.i18n_select_an_option );
			}

			this._is_optional = optional;

		};

		/**
		 * Initialize quantity input
		 */

		this.init_qty_input = function() {

			// Quantity buttons
			if ( wc_composite_params.is_wc_version_gte_2_3 === 'no' || wc_composite_params.show_quantity_buttons === 'yes' ) {
				this.$self.find( 'div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<input type="button" value="+" class="plus" />' ).prepend( '<input type="button" value="-" class="minus" />' );
			}

			// Target quantity inputs on product pages
			this.$self.find( '.component_wrap input.qty' ).each( function() {

				var min = parseFloat( $(this).attr( 'min' ) );

				if ( min >= 0 && parseFloat( $(this).val() ) < min ) {
					$(this).val( min );
				}

			} );

		};

	}

	/*
	 * Initialize form script
	 */

	$( '.composite_form .composite_data' ).each( function() {
		$(this).wc_composite_form();
	} );

} );
