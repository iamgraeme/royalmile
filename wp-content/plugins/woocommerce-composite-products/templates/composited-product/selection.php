<?php
/**
 * Composited product selection template.
 *
 * @version  3.2.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<p class="component_section_title">
	<label class="selected_option_label"><?php echo __( 'Your selection:', 'woocommerce-composite-products' ); ?></label>
</p>
